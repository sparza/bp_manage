sap.ui.define([
	"sap/base/Log",
	"bp/spar/co/za/BusinessPartner/services/LookupService"
], function (Log, LookupService) {
	"use strict";

	return {
		buildingBPToApprovalView: function (that, inBpData) {

			if (inBpData.generalData.liquorLicenceNumberStartDate) {
				var liquorLicenceNumberStartDate = inBpData.generalData.liquorLicenceNumberStartDate;
				inBpData.generalData.liquorLicenceNumberStartDate = liquorLicenceNumberStartDate.getDate() + "/" + liquorLicenceNumberStartDate.getMonth() +
					"/" + liquorLicenceNumberStartDate.getFullYear();
			}

			if (inBpData.generalData.liquorLicenceNumberEndDate) {
				var liquorLicenceNumberEndDate = inBpData.generalData.liquorLicenceNumberEndDate;
				inBpData.generalData.liquorLicenceNumberEndDate = liquorLicenceNumberEndDate.getDate() + "/" + liquorLicenceNumberEndDate.getMonth() +
					"/" + liquorLicenceNumberEndDate.getFullYear();
			}
			////*******

			if (inBpData.generalData.grocerWineLicenceNumberStartDate) {
				var grocerWineLicenceNumberStartDate = inBpData.generalData.grocerWineLicenceNumberStartDate;
				inBpData.generalData.grocerWineLicenceNumberStartDate = grocerWineLicenceNumberStartDate.getDate() + "/" +
					grocerWineLicenceNumberStartDate.getMonth() +
					"/" + grocerWineLicenceNumberStartDate.getFullYear();
			}

			if (inBpData.generalData.grocerWineLicenceNumberEndDate) {
				var grocerWineLicenceNumberEndDate = inBpData.generalData.grocerWineLicenceNumberEndDate;
				inBpData.generalData.grocerWineLicenceNumberEndDate = grocerWineLicenceNumberEndDate.getDate() + "/" +
					grocerWineLicenceNumberEndDate.getMonth() +
					"/" + grocerWineLicenceNumberEndDate.getFullYear();
			}
			////*********	

			if (inBpData.generalData.distributionLicencNumberStartDate) {
				var distributionLicencNumberStartDate = inBpData.generalData.distributionLicencNumberStartDate;
				inBpData.generalData.distributionLicencNumberStartDate = distributionLicencNumberStartDate.getDate() + "/" +
					distributionLicencNumberStartDate.getMonth() +
					"/" + distributionLicencNumberStartDate.getFullYear();
			}

			if (inBpData.generalData.distributionLicencNumberEndDate) {
				var distributionLicencNumberEndDate = inBpData.generalData.distributionLicencNumberEndDate;
				inBpData.generalData.distributionLicencNumberEndDate = distributionLicencNumberEndDate.getDate() + "/" +
					distributionLicencNumberEndDate.getMonth() +
					"/" + distributionLicencNumberEndDate.getFullYear();
			}

			// debugger;
			try {
				inBpData.generalData.countryGroupDetail = that.getView().byId("idCountryGroupDetail").getSelectedItem().getProperty("text");
			} catch (error) {
				inBpData.generalData.countryGroupDetail = "";
			}
			try {
				inBpData.address.addressTypePAddress = that.getView().byId("idAddressTypePAddress").getSelectedItem().getProperty("text");
			} catch (error) {
				inBpData.address.addressTypePAddress = "";
			}
			try {
				inBpData.address.provinceSAddres = that.getView().byId("idProvinceSAddress").getSelectedItem().getProperty("text");
			} catch (error) {
				inBpData.address.provinceSAddres = "";
			}
			try {
				inBpData.address.countrySAddress = that.getView().byId("idCountrySAddress").getSelectedItem().getProperty("text");
			} catch (error) {
				inBpData.address.countrySAddress = "";
			}
			try {
				inBpData.address.provincePAddress = that.getView().byId("idProvincePAddress").getSelectedItem().getProperty("text");
			} catch (error) {
				inBpData.address.provincePAddress = "";
			}
			try {
				inBpData.address.countryPAddress = that.getView().byId("idCountryPAddress").getSelectedItem().getProperty("text");
			} catch (error) {
				inBpData.address.countryPAddress = "";
			}
			///******Bank*****//
			var inBpDataBank = inBpData.bankData;
			// that.getView().getModel("pModel").getProperty("/bankData");
			if (inBpDataBank) {
				// var arrayBank = (this.getView().byId("idBankDetail").getRows().length) - 1;
				for (var i = 0; i < inBpDataBank.length; i++) {
					try {
						inBpData.bankData[i].countryBankDetail = that.getView().byId("idBankDetail").getRows()[i].getCells()[0].getSelectedItem()
							.getProperty("text");
					} catch (error) {
						inBpData.bankData[i].countryBankDetail = "";
					}
				}
			}
			///***********Company*******///
			var inBpDataCompany = inBpData.company;
			// that.getView().getModel("pModel").getProperty("/company");

			if (inBpDataCompany) {
				for (var iCompany = 0; iCompany < inBpDataCompany.length; iCompany++) {
					try {
						inBpData.company[iCompany].paymentTerms = that.getView().byId("idCompanyDetail").getRows()[iCompany].getCells()[1].getSelectedItem()
							.getProperty("text");
					} catch (error) {
						inBpData.company[iCompany].paymentTerms = "";
					}

					try {
						inBpData.company[iCompany].paymentMethods = that.getView().byId("idCompanyDetail").getRows()[iCompany].getCells()[2]
							.getSelectedItem().getProperty("text");
					} catch (error) {
						inBpData.company[iCompany].paymentMethods = "";
					}
				}
			}
			////////******************************************************************//////
			return inBpData;
		},
		buildServiceObjectMapFromView_Create: function (data) {
			var respData = {};
			respData.generalData = {};
			respData.contacts = {};
			respData.address = {};
			respData.bpData = [];
			///********************************************
			///converting mandatory fields to upper case
			data.generalData.nameGroupDetail.toUpperCase();
			data.generalData.entityRegistrationNumber.toUpperCase();
			data.address.streetNameSAddress.toUpperCase();
			data.address.citySAddress.toUpperCase();
			data.address.postalCodeSAddress.toUpperCase();
			///********************************************
			var BusinessPartner = data.generalData.code;

			////////________________________________________________//////
			var OrganizationBPName1 = data.generalData.nameGroupDetail;
			var SearchTerm1 = data.generalData.searchTerm1;
			var BusinessPartnerIsBlocked = false;
			var BusinessPartnerCategory = "2";
			var BusinessPartnerGrouping = data.generalData.group;
			// var BPIdentificationType_1 = "Z001";
			// var BPIdentificationType_2 = "Z002";
			// var BPIdentificationType_3 = "Z003";
			// var BPIdentificationNumber = "ABVKD93498";
			////**********************Start of Address data**************/////

			var complexNumberSAddress = data.address.complexNumberSAddress ? data.address.complexNumberSAddress : "";
			// function () {
			// 	if (data.address.complexNumberSAddress)
			// 		return data.address.complexNumberSAddress;
			// 	else return "";
			// };
			var streetNumberSAddress = data.address.streetNumberSAddress;
			var streetNameSAddress = data.address.streetNameSAddress;
			var subuRbSAddress = data.address.subuRbSAddress;
			var citySAddress = data.address.citySAddress;
			var postalCodeSAddress = data.address.postalCodeSAddress;
			var provinceSAddres = data.address.provinceSAddres;
			var countrySAddress = data.address.countrySAddress;
			var PrfrdCommMediumType = "INT";
			var StreetPrefixName = "";

			var addressTypePAddress = data.address.addressTypePAddress;
			var addressNumberPAddress = data.address.addressNumberPAddress;
			var cityPAddress = data.address.cityPAddress;
			var postalCodePAddress = data.address.postalCodePAddress;
			var provincePAddress = data.address.provincePAddress;
			var countryPAddress = data.address.countryPAddress;
			var AddressUsage = "XXDEFAULT";
			//////*********************End of Address Data*****************/////

			//////*********************Start of Contact Data*****************/////

			///**********Creating Telephone Object-to_PhoneNumber***********///

			// var telephoneArray = [];

			var to_PhoneNumber = [],
				to_FaxNumber = [],
				to_EmailAddress = [],
				to_MobilePhoneNumber = [];

			if (data.contacts) {
				var telephoneArray = {};
				if (data.contacts.telephoneData) {
					telephoneArray = data.contacts.telephoneData;
					for (var iPhone = 0; iPhone < telephoneArray.length; iPhone++) {
						to_PhoneNumber[iPhone] = {};
						to_PhoneNumber[iPhone].PhoneNumberType = "1";
						to_PhoneNumber[iPhone].IsDefaultPhoneNumber = telephoneArray[iPhone].primary;
						to_PhoneNumber[iPhone].PhoneNumber = telephoneArray[iPhone].telephoneNo;
						to_PhoneNumber[iPhone].Person = telephoneArray[iPhone].telephoneName;
						to_PhoneNumber[iPhone].PhoneNumberExtension = telephoneArray[iPhone].telephoneExtension;
						to_PhoneNumber[iPhone].InternationalPhoneNumber = "+" + telephoneArray[iPhone].telephoneDialingCode + "" + telephoneArray[iPhone]
							.telephoneNo;
						to_PhoneNumber[iPhone].PhoneNumberType = telephoneArray[iPhone].idTelephoneTabContactType;
						to_PhoneNumber[iPhone].OrdinalNumber = iPhone.toString();
					}
				}
				///**********Creating Cellphone Object-to_PhoneNumber***********///
				////------------------------------------------------------------////
				if (data.contacts.cellphoneData) {
					var cellphoneArray = data.contacts.cellphoneData;
					for (var iCellphone = 0; iCellphone < cellphoneArray.length; iCellphone++) {
						to_MobilePhoneNumber[iCellphone] = {};
						to_MobilePhoneNumber[iCellphone].PhoneNumberType = "3";
						to_MobilePhoneNumber[iCellphone].IsDefaultPhoneNumber = cellphoneArray[iCellphone].primary;
						to_MobilePhoneNumber[iCellphone].Person = cellphoneArray[iCellphone].cellphoneName;
						to_MobilePhoneNumber[iCellphone].PhoneNumber = cellphoneArray[iCellphone].cellphoneNo;
						to_MobilePhoneNumber[iCellphone].InternationalPhoneNumber = "+" + cellphoneArray[iCellphone].cellphoneDialingCode + "" +
							cellphoneArray[
								iCellphone]
							.cellphoneNo;
						// to_MobilePhoneNumber[iCellphone].PhoneNumberType = cellphoneArray[iCellphone].cellphoneContactType;
						to_MobilePhoneNumber[iCellphone].OrdinalNumber = iCellphone.toString();
					}
				}
				///**********Creating Fax Object-to_FaxNumber***********///
				////------------------------------------------------------------////
				if (data.contacts.faxData) {
					var faxArray = data.contacts.faxData;
					for (var iFax = 0; iFax < faxArray.length; iFax++) {
						to_FaxNumber[iFax] = {};
						to_FaxNumber[iFax].IsDefaultFaxNumber = faxArray[iFax].primary;
						to_FaxNumber[iFax].FaxNumber = faxArray[iFax].faxNo;
						to_FaxNumber[iFax].Person = faxArray[iFax].faxName;
						to_FaxNumber[iFax].InternationalFaxNumber = "+" + faxArray[iFax].faxDialingCode + "" + faxArray[iFax].faxNo;
						to_FaxNumber[iFax].OrdinalNumber = iFax.toString();
					}
				}
				///**********Creating Eail Object-to_EmailAddress***********///
				////------------------------------------------------------------////
				if (data.contacts.emailData) {
					var emailArray = data.contacts.emailData;
					for (var iEmail = 0; iEmail < emailArray.length; iEmail++) {
						to_EmailAddress[iEmail] = {};
						to_EmailAddress[iEmail].IsDefaultEmailAddress = emailArray[iEmail].primary;
						to_EmailAddress[iEmail].Person = emailArray[iEmail].emailName;
						to_EmailAddress[iEmail].EmailAddress = emailArray[iEmail].emailId;
						to_EmailAddress[iEmail].SearchEmailAddress = emailArray[iEmail].emailId;
						to_EmailAddress[iEmail].OrdinalNumber = iEmail.toString();
					}
				}
			}
			///////*****************************Bank Detail Section**********************///
			/////___________________________________________________________________///////

			var to_BusinessPartnerBank = [];
			var bankData = data.bankData;
			if (bankData) {
				for (var iBank = 0; iBank < bankData.length; iBank++) {
					to_BusinessPartnerBank[iBank] = {};
					to_BusinessPartnerBank[iBank].BusinessPartner = BusinessPartner;
					var bankId = iBank.toString();
					if (bankId.length === 1) {
						bankId = "000" + bankId;
					} else if (bankId.length === 2) {
						bankId = "00" + bankId;
					}
					to_BusinessPartnerBank[iBank].BankIdentification = bankId; //"0001"; //iBank;
					to_BusinessPartnerBank[iBank].BankCountryKey = bankData[iBank].countryBankDetail;
					to_BusinessPartnerBank[iBank].BankName = bankData[iBank].bankNameBank;
					to_BusinessPartnerBank[iBank].BankNumber = bankData[iBank].bankCodeBank;
					to_BusinessPartnerBank[iBank].BankAccountHolderName = bankData[iBank].accountNameBank;
					to_BusinessPartnerBank[iBank].BankAccount = bankData[iBank].accountNumberBank;
					to_BusinessPartnerBank[iBank].CollectionAuthInd = true;
				}
			}
			///**************Business Partner Identification - to_BuPaIdentification*************////
			////_______________________________________________________________________/////

			var to_BuPaIdentification = [];

			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyy-MM-dd"
			});

			//*********Store Liquor License
			if (data.generalData.liquorLicenceNumber) {
				to_BuPaIdentification[0] = {};
				to_BuPaIdentification[0].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[0].BPIdentificationType = "Z001";
				to_BuPaIdentification[0].BPIdentificationNumber = data.generalData.liquorLicenceNumber;
				to_BuPaIdentification[0].BPIdnNmbrIssuingInstitute = "";

				// var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				// 	pattern: "yyyy-MM-dd"
				// });
				var liquorLicenceEntryDate = dateFormat.format(new Date());
				liquorLicenceEntryDate = liquorLicenceEntryDate + "T00:00:00";
				to_BuPaIdentification[0].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceEntryDate; //new Date("24.05.2020");
				// debugger;
				// var vStartDate = data.liquorLicenceNumberStartDate.getDate() + "." + data.liquorLicenceNumberStartDate.getMonth() + "." + data.generalData.liquorLicenceNumberStartDate
				// 	.getFullYear();
				var liquorLicenceStartDate = dateFormat.format(data.generalData.liquorLicenceNumberStartDate);
				liquorLicenceStartDate = liquorLicenceStartDate + "T00:00:00";
				to_BuPaIdentification[0].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceStartDate; //new Date(liquorLicenceStartDate); //data.generalData.liquorLicenceNumberStartDate;

				to_BuPaIdentification[0].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //new Date("17.05.2020"); //data.generalData.liquorLicenceNumberEndDate;
				to_BuPaIdentification[0].Country = "";
				to_BuPaIdentification[0].Region = "";
				to_BuPaIdentification[0].AuthorizationGroup = "";
			}
			//*********Grocer's Wine License
			if (data.generalData.grocerWineLicenceNumber) {
				to_BuPaIdentification[1] = {};
				to_BuPaIdentification[1].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[1].BPIdentificationType = "Z002";
				to_BuPaIdentification[1].BPIdentificationNumber = data.generalData.grocerWineLicenceNumber;
				to_BuPaIdentification[1].BPIdnNmbrIssuingInstitute = "";
				to_BuPaIdentification[1].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
				// to_BuPaIdentification[1].BPIdentificationEntryDate = new Date();
				// data.grocerWineLicenceNumberStartDate
				// var wineLicenceStartDate = dateFormat.format(new Date()) + "T00:00:00";
				to_BuPaIdentification[1].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //wineLicenceStartDate; //new Date(wineLicenceStartDate); //oDateFormat.format(data.generalData.grocerWineLicenceNumberStartDate);
				to_BuPaIdentification[1].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.grocerWineLicenceNumberEndDate;
				to_BuPaIdentification[1].Country = "";
				to_BuPaIdentification[1].Region = "";
				to_BuPaIdentification[1].AuthorizationGroup = "";
			}
			//*********Distribution License
			if (data.generalData.distributionLicencNumber) {
				to_BuPaIdentification[2] = {};
				to_BuPaIdentification[2].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[2].BPIdentificationType = "Z003";
				to_BuPaIdentification[2].BPIdentificationNumber = data.generalData.distributionLicencNumber;
				to_BuPaIdentification[2].BPIdnNmbrIssuingInstitute = "";
				to_BuPaIdentification[2].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
				// // to_BuPaIdentification[2].BPIdentificationEntryDate = new Date();
				to_BuPaIdentification[2].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberStartDate;
				to_BuPaIdentification[2].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberEndDate;
				to_BuPaIdentification[2].Country = "";
				to_BuPaIdentification[2].Region = "";
				to_BuPaIdentification[2].AuthorizationGroup = "";
			}

			// if (data.)
			///**************Business Partner Role - to_BusinessPartnerRole*************////
			////_______________________________________________________________________/////
			// var to_BusinessPartnerRole = this._businessPartnerRole(BusinessPartner);

			///**************Business Partner Contact - to_BusinessPartnerContact*************////
			////_______________________________________________________________________/////
			// var to_BusinessPartnerContact = [];

			///**************Business Partner Tax - to_BusinessPartnerTax*************////
			////_______________________________________________________________________/////			
			var to_BusinessPartnerTax = [];
			var bpTaxCount = 0;
			if (data.generalData.vatRegistrationNo) {
				// bpTaxCount++;
				to_BusinessPartnerTax[bpTaxCount] = {};
				to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
				to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA1";
				to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = data.generalData.vatRegistrationNo;
			}
			if (data.generalData.entityRegistrationNumber) {
				bpTaxCount++;
				to_BusinessPartnerTax[bpTaxCount] = {};
				to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
				to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA4";
				to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = data.generalData.entityRegistrationNumber;
			}
			////------------Customer Object - to_Customer--------------------////
			////_______________Customer Company Variable______________//////
			// debugger;
			var to_CustomerCompany = [];
			var to_CustomerSalesArea = [];
			var Company = data.company;
			if (Company) {
				for (var cCount = 0; cCount < Company.length; cCount++) {
					////*********Coding to create the company
					to_CustomerCompany[cCount] = {};
					to_CustomerCompany[cCount].Customer = BusinessPartner;
					to_CustomerCompany[cCount].CompanyCode = Company[cCount].companyCode;
					to_CustomerCompany[cCount].AccountingClerkInternetAddress = Company[cCount].accountingClerkEmails;
					to_CustomerCompany[cCount].CustomerAccountNote = "PAYMENT REF STORE";
					to_CustomerCompany[cCount].CustomerSupplierClearingIsUsed = false;
					to_CustomerCompany[cCount].InterestCalculationCode = "Z1";
					to_CustomerCompany[cCount].IntrstCalcFrequencyInMonths = "0";
					to_CustomerCompany[cCount].IsToBeLocallyProcessed = false;
					to_CustomerCompany[cCount].ItemIsToBePaidSeparately = false;
					to_CustomerCompany[cCount].LayoutSortingRule = "001";
					to_CustomerCompany[cCount].PaymentMethodsList = Company[cCount].paymentMethods;
					to_CustomerCompany[cCount].PaymentTerms = Company[cCount].paymentTerms;
					to_CustomerCompany[cCount].PaytAdviceIsSentbyEDI = false;
					to_CustomerCompany[cCount].PhysicalInventoryBlockInd = false;
					to_CustomerCompany[cCount].ReconciliationAccount = Company[cCount].reconciliationAccount;
					to_CustomerCompany[cCount].RecordPaymentHistoryIndicator = true;
					to_CustomerCompany[cCount].UserAtCustomer = Company[cCount].userAtCustomer;
					to_CustomerCompany[cCount].DeletionIndicator = false;

					// if(to_CustomerCompany[cCount].CompanyCode === Managing DC){
					// to_CustomerCompany[cCount].to_CustomerDunning = {};
					// to_CustomerCompany[cCount].to_WithHoldingTax = {};
					// }
					// else{
					to_CustomerCompany[cCount].to_CustomerDunning = [];
					to_CustomerCompany[cCount].to_WithHoldingTax = [];
					// }
					////***********Coding to create the sales area
					to_CustomerSalesArea[cCount] = {};
					to_CustomerSalesArea[cCount].Customer = BusinessPartner;
					to_CustomerSalesArea[cCount].SalesOrganization = Company[cCount].companyCode;
					to_CustomerSalesArea[cCount].DistributionChannel = "95";
					to_CustomerSalesArea[cCount].Division = "01";
					to_CustomerSalesArea[cCount].CompleteDeliveryIsDefined = false;
					to_CustomerSalesArea[cCount].Currency = "ZAR";
					to_CustomerSalesArea[cCount].CustomerPaymentTerms = Company[cCount].paymentTerms;
					to_CustomerSalesArea[cCount].CustomerPricingProcedure = "1";
					to_CustomerSalesArea[cCount].DeliveryPriority = "2";
					to_CustomerSalesArea[cCount].DeletionIndicator = false;
					to_CustomerSalesArea[cCount].ItemOrderProbabilityInPercent = "0";
					to_CustomerSalesArea[cCount].OrderCombinationIsAllowed = true;
					to_CustomerSalesArea[cCount].ShippingCondition = "01";
					///Codeing for the partner function in sales area
					to_CustomerSalesArea[cCount].to_PartnerFunction = [];
					for (var sCount = 0; sCount < 4; sCount++) {
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount] = {};
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Customer = BusinessPartner;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].SalesOrganization = Company[cCount].companyCode;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DistributionChannel = "95";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Division = "01";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].PartnerCounter = "0";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].BPCustomerNumber = BusinessPartner;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].CustomerPartnerDescription = "";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DefaultPartner = false;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].AuthorizationGroup = "";
					}
					to_CustomerSalesArea[cCount].to_PartnerFunction[0].PartnerFunction = "SP";
					to_CustomerSalesArea[cCount].to_PartnerFunction[1].PartnerFunction = "BP";
					to_CustomerSalesArea[cCount].to_PartnerFunction[2].PartnerFunction = "PY";
					to_CustomerSalesArea[cCount].to_PartnerFunction[3].PartnerFunction = "SH";

					//Codeing for the sales tax area in sales area	
					to_CustomerSalesArea[cCount].to_SalesAreaTax = [];
					for (var taxCount = 0; taxCount < 5; taxCount++) {
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount] = {};
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Customer = BusinessPartner;
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].SalesOrganization = Company[cCount].companyCode;
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].DistributionChannel = "95";
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Division = "01";
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].CustomerTaxCategory = "MWST";

					}
					to_CustomerSalesArea[cCount].to_SalesAreaTax[0].DepartureCountry = "SG";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[1].DepartureCountry = "MZ";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[2].DepartureCountry = "NA";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[3].DepartureCountry = "ZA";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[4].DepartureCountry = "BW";
				}
			}
			// var LayoutSortingRule = "001";
			// var RecordPaymentHistoryIndicator = true;
			// "Customer": "80314",
			// "CompanyCode": "1061",
			// "AccountingClerk": "",
			// "AccountByCustomer": "",
			// "LayoutSortingRule": "001",
			// "ReconciliationAccount": "140000",
			// "PaymentTerms": "Z30D",
			// "PaymentMethodsList": "D",
			//"RecordPaymentHistoryIndicator": true,
			// "UserAtCustomer": "CLERK 1",
			// "AccountingClerkInternetAddress": "manager@knowlesops.co.za"
			/////___________________________________________________//////
			//////_______________Customer Sales Area________________/////
			// var CustomerSalesAreaPartnerFunction = [];
			// var CustomerSalesAreaTax = [];
			// var CustomerSalesArea = [];
			//////__________________________________________________//////
			var to_Customer = {};
			to_Customer.Customer = BusinessPartner;
			to_Customer.DeletionIndicator = false;
			to_Customer.CustomerAccountGroup = data.generalData.group;
			to_Customer.CustomerFullName = OrganizationBPName1;
			to_Customer.CustomerName = OrganizationBPName1;
			to_Customer.PostingIsBlocked = false;
			to_Customer.InternationalLocationNumber1 = "0";
			to_Customer.TaxNumber1 = data.generalData.vatRegistrationNo;
			to_Customer.TaxNumber3 = data.generalData.entityRegistrationNumber;
			to_Customer.DeletionIndicator = false;
			to_Customer.to_CustomerCompany = to_CustomerCompany;
			to_Customer.to_CustomerSalesArea = [];
			// to_CustomerSalesArea;

			var request = {
				BusinessPartner: BusinessPartner,
				Customer: BusinessPartner,
				Supplier: BusinessPartner,
				BusinessPartnerCategory: BusinessPartnerCategory,
				BusinessPartnerFullName: OrganizationBPName1,
				BusinessPartnerGrouping: BusinessPartnerGrouping,
				BusinessPartnerName: OrganizationBPName1,
				InternationalLocationNumber1: "0",
				InternationalLocationNumber2: "0",
				IsFemale: false,
				IsMale: false,
				IsSexUnknown: false,
				OrganizationBPName1: OrganizationBPName1,
				OrganizationBPName4: OrganizationBPName1,
				SearchTerm1: SearchTerm1,
				BusinessPartnerIsBlocked: BusinessPartnerIsBlocked,
				BusinessPartnerType: "0002",
				InternationalLocationNumber3: "0",
				IsMarkedForArchiving: false,
				to_BuPaIdentification: to_BuPaIdentification,

				to_BusinessPartnerAddress: [{
					BusinessPartner: BusinessPartner,
					CityName: citySAddress,
					Country: countrySAddress,
					District: subuRbSAddress,
					FullName: OrganizationBPName1,
					HouseNumber: complexNumberSAddress,
					Language: "EN",
					PostalCode: postalCodeSAddress,
					PrfrdCommMediumType: PrfrdCommMediumType,
					Region: provinceSAddres,
					StreetName: streetNameSAddress,
					StreetPrefixName: StreetPrefixName,
					////**********Postal Address*******///
					DeliveryServiceNumber: addressNumberPAddress,
					DeliveryServiceTypeCode: addressTypePAddress,
					POBoxPostalCode: postalCodePAddress,
					POBoxDeviatingCityName: cityPAddress,
					POBoxDeviatingRegion: provincePAddress,
					POBoxDeviatingCountry: countryPAddress,
					to_AddressUsage: [{
						BusinessPartner: BusinessPartner,
						//ValidityEndDate:"9999-01-01T00:00:00Z",
						AddressUsage: AddressUsage
							//ValidityStartDate":"2018-09-21T15:20:15Z"
					}],

					to_PhoneNumber: to_PhoneNumber,
					to_MobilePhoneNumber: to_MobilePhoneNumber,
					to_FaxNumber: to_FaxNumber,
					to_EmailAddress: to_EmailAddress
				}],
				to_BusinessPartnerBank: to_BusinessPartnerBank,
				// var to_BusinessPartnerRole = ;
				to_BusinessPartnerRole: this._businessPartnerRole(BusinessPartner),
				// to_BusinessPartnerContact: to_BusinessPartnerContact,
				to_BusinessPartnerTax: to_BusinessPartnerTax,
				to_Customer: to_Customer
			};
			return request;
		},
		// buildServiceObjectMapFromView_Update: function (data, orgData) {
		// 	var respData = {};
		// 	respData.generalData = {};
		// 	respData.contacts = {};
		// 	respData.address = {};
		// 	respData.bpData = [];
		// 	///********************************************
		// 	///converting mandatory fields to upper case
		// 	data.generalData.nameGroupDetail.toUpperCase();
		// 	data.generalData.entityRegistrationNumber.toUpperCase();
		// 	data.address.streetNameSAddress.toUpperCase();
		// 	data.address.citySAddress.toUpperCase();
		// 	data.address.postalCodeSAddress.toUpperCase();
		// 	///********************************************
		// 	var BusinessPartner = data.generalData.code;

		// 	////////________________________________________________//////
		// 	var OrganizationBPName1 = data.generalData.nameGroupDetail;
		// 	var SearchTerm1 = data.generalData.searchTerm1;
		// 	var BusinessPartnerIsBlocked = false;
		// 	var BusinessPartnerCategory = "2";
		// 	var BusinessPartnerGrouping = data.generalData.group;
		// 	// var BPIdentificationType_1 = "Z001";
		// 	// var BPIdentificationType_2 = "Z002";
		// 	// var BPIdentificationType_3 = "Z003";
		// 	// var BPIdentificationNumber = "ABVKD93498";
		// 	////**********************Start of Address data**************/////

		// 	var complexNumberSAddress =
		// 		function () {
		// 			if (data.address.complexNumberSAddress)
		// 				return data.address.complexNumberSAddress;
		// 			else return "";
		// 		};
		// 	var streetNumberSAddress = data.address.streetNumberSAddress;
		// 	var streetNameSAddress = data.address.streetNameSAddress;
		// 	var subuRbSAddress = data.address.subuRbSAddress;
		// 	var citySAddress = data.address.citySAddress;
		// 	var postalCodeSAddress = data.address.postalCodeSAddress;
		// 	var provinceSAddres = data.address.provinceSAddres;
		// 	var countrySAddress = data.address.countrySAddress;
		// 	var PrfrdCommMediumType = "INT";
		// 	var StreetPrefixName = "";

		// 	var addressTypePAddress = data.address.addressTypePAddress;
		// 	var addressNumberPAddress = data.address.addressNumberPAddress;
		// 	var cityPAddress = data.address.cityPAddress;
		// 	var postalCodePAddress = data.addresspostalCodePAddress;
		// 	var provincePAddress = data.address.provincePAddress;
		// 	var countryPAddress = data.address.countryPAddress;
		// 	var AddressUsage = "XXDEFAULT";
		// 	//////*********************End of Address Data*****************/////

		// 	//////*********************Start of Contact Data*****************/////

		// 	///**********Creating Telephone Object-to_PhoneNumber***********///

		// 	// var telephoneArray = [];

		// 	var to_PhoneNumber = [];
		// 	var to_FaxNumber = [];
		// 	var to_EmailAddress = [];
		// 	var to_MobilePhoneNumber = [];

		// 	if (data.contacts) {
		// 		var iPhone = 0;
		// 		var telephoneArray = {};
		// 		if (data.contacts.telephoneData) {
		// 			telephoneArray = data.contacts.telephoneData;
		// 			for (; iPhone < telephoneArray.length; iPhone++) {
		// 				to_PhoneNumber[iPhone] = {};
		// 				to_PhoneNumber[iPhone].PhoneNumberType = "1";
		// 				to_PhoneNumber[iPhone].IsDefaultPhoneNumber = telephoneArray[iPhone].primary;
		// 				to_PhoneNumber[iPhone].PhoneNumber = telephoneArray[iPhone].telephoneNo;
		// 				to_PhoneNumber[iPhone].Person = telephoneArray[iPhone].telephoneName;
		// 				to_PhoneNumber[iPhone].PhoneNumberExtension = telephoneArray[iPhone].telephoneExtension;
		// 				to_PhoneNumber[iPhone].InternationalPhoneNumber = "+" + telephoneArray[iPhone].telephoneDialingCode + "" + telephoneArray[iPhone]
		// 					.telephoneNo;
		// 				to_PhoneNumber[iPhone].PhoneNumberType = telephoneArray[iPhone].idTelephoneTabContactType;
		// 				to_PhoneNumber[iPhone].OrdinalNumber = iPhone.toString();
		// 			}
		// 		}
		// 		///**********Creating Cellphone Object-to_PhoneNumber***********///
		// 		////------------------------------------------------------------////
		// 		if (data.contacts.cellphoneData) {
		// 			var cellphoneArray = data.contacts.cellphoneData;

		// 			var totalLength = 0;
		// 			// if (telephoneArray) {
		// 			// totalLength = telephoneArray.length + cellphoneArray.length;
		// 			totalLength = cellphoneArray.length;
		// 			// } else {
		// 			// 	totalLength = cellphoneArray.length;
		// 			// }
		// 			iPhone = 0;
		// 			for (; iPhone < totalLength; iPhone++) {
		// 				to_MobilePhoneNumber[iPhone] = {};
		// 				to_MobilePhoneNumber[iPhone].PhoneNumberType = "3";
		// 				to_MobilePhoneNumber[iPhone].IsDefaultPhoneNumber = cellphoneArray[iPhone].primary;
		// 				to_MobilePhoneNumber[iPhone].Person = cellphoneArray[iPhone].cellphoneName;
		// 				to_MobilePhoneNumber[iPhone].PhoneNumber = cellphoneArray[iPhone].cellphoneNo;
		// 				to_MobilePhoneNumber[iPhone].InternationalPhoneNumber = "+" + cellphoneArray[iPhone].cellphoneDialingCode + "" + cellphoneArray[
		// 						iPhone]
		// 					.cellphoneNo;
		// 				// to_MobilePhoneNumber[iPhone].PhoneNumberType = cellphoneArray[cPhone].cellphoneContactType;
		// 				to_MobilePhoneNumber[iPhone].OrdinalNumber = iPhone.toString();
		// 			}
		// 		}
		// 		///**********Creating Fax Object-to_FaxNumber***********///
		// 		////------------------------------------------------------------////
		// 		if (data.contacts.faxData) {
		// 			var faxArray = data.contacts.faxData;
		// 			for (var iFax = 0; iFax < faxArray.length; iFax++) {
		// 				to_FaxNumber[iFax] = {};
		// 				to_FaxNumber[iFax].IsDefaultFaxNumber = faxArray[iFax].primary;
		// 				to_FaxNumber[iFax].FaxNumber = faxArray[iFax].faxNo;
		// 				to_FaxNumber[iFax].Person = faxArray[iFax].faxName;
		// 				to_FaxNumber[iFax].InternationalFaxNumber = "+" + faxArray[iFax].faxDialingCode + "" + faxArray[iFax].faxNo;
		// 				to_FaxNumber[iFax].OrdinalNumber = iFax.toString();
		// 			}
		// 		}
		// 		///**********Creating Eail Object-to_EmailAddress***********///
		// 		////------------------------------------------------------------////
		// 		if (data.contacts.emailData) {
		// 			var emailArray = data.contacts.emailData;
		// 			for (var iEmail = 0; iEmail < emailArray.length; iEmail++) {
		// 				to_EmailAddress[iEmail] = {};
		// 				to_EmailAddress[iEmail].IsDefaultEmailAddress = emailArray[iEmail].primary;
		// 				to_EmailAddress[iEmail].Person = emailArray[iEmail].emailName;
		// 				to_EmailAddress[iEmail].EmailAddress = emailArray[iEmail].emailId;
		// 				to_EmailAddress[iEmail].SearchEmailAddress = emailArray[iEmail].emailId;
		// 				to_EmailAddress[iEmail].OrdinalNumber = iEmail.toString();
		// 			}
		// 		}
		// 	}
		// 	///////*****************************Bank Detail Section**********************///
		// 	/////___________________________________________________________________///////

		// 	var to_BusinessPartnerBank = [];
		// 	var bankData = data.bankData;
		// 	if (bankData) {
		// 		for (var iBank = 0; iBank < bankData.length; iBank++) {
		// 			to_BusinessPartnerBank[iBank] = {};
		// 			to_BusinessPartnerBank[iBank].BusinessPartner = BusinessPartner;
		// 			var bankId = iBank.toString();
		// 			if (bankId.length === 1) {
		// 				bankId = "000" + bankId;
		// 			} else if (bankId.length === 2) {
		// 				bankId = "00" + bankId;
		// 			}
		// 			to_BusinessPartnerBank[iBank].BankIdentification = bankId; //"0001"; //iBank;
		// 			to_BusinessPartnerBank[iBank].BankCountryKey = bankData[iBank].countryBankDetail;
		// 			to_BusinessPartnerBank[iBank].BankName = bankData[iBank].bankNameBank;
		// 			to_BusinessPartnerBank[iBank].BankNumber = bankData[iBank].bankCodeBank;
		// 			to_BusinessPartnerBank[iBank].BankAccountHolderName = bankData[iBank].accountNameBank;
		// 			to_BusinessPartnerBank[iBank].BankAccount = bankData[iBank].accountNumberBank;
		// 			to_BusinessPartnerBank[iBank].CollectionAuthInd = true;
		// 		}
		// 	}
		// 	///**************Business Partner Identification - to_BuPaIdentification*************////
		// 	////_______________________________________________________________________/////

		// 	var to_BuPaIdentification = [];

		// 	var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
		// 		pattern: "yyyy-MM-dd"
		// 	});

		// 	//*********Store Liquor License
		// 	if (data.generalData.liquorLicenceNumber) {
		// 		to_BuPaIdentification[0] = {};
		// 		to_BuPaIdentification[0].BusinessPartner = BusinessPartner;
		// 		to_BuPaIdentification[0].BPIdentificationType = "Z001";
		// 		to_BuPaIdentification[0].BPIdentificationNumber = data.generalData.liquorLicenceNumber;
		// 		to_BuPaIdentification[0].BPIdnNmbrIssuingInstitute = "";

		// 		// var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
		// 		// 	pattern: "yyyy-MM-dd"
		// 		// });
		// 		var liquorLicenceEntryDate = dateFormat.format(new Date());
		// 		liquorLicenceEntryDate = liquorLicenceEntryDate + "T00:00:00";
		// 		to_BuPaIdentification[0].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceEntryDate; //new Date("24.05.2020");
		// 		// debugger;
		// 		// var vStartDate = data.liquorLicenceNumberStartDate.getDate() + "." + data.liquorLicenceNumberStartDate.getMonth() + "." + data.generalData.liquorLicenceNumberStartDate
		// 		// 	.getFullYear();
		// 		var liquorLicenceStartDate = dateFormat.format(data.generalData.liquorLicenceNumberStartDate);
		// 		liquorLicenceStartDate = liquorLicenceStartDate + "T00:00:00";
		// 		to_BuPaIdentification[0].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceStartDate; //new Date(liquorLicenceStartDate); //data.generalData.liquorLicenceNumberStartDate;

		// 		to_BuPaIdentification[0].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //new Date("17.05.2020"); //data.generalData.liquorLicenceNumberEndDate;
		// 		to_BuPaIdentification[0].Country = "";
		// 		to_BuPaIdentification[0].Region = "";
		// 		to_BuPaIdentification[0].AuthorizationGroup = "";
		// 	}
		// 	//*********Grocer's Wine License
		// 	if (data.generalData.grocerWineLicenceNumber) {
		// 		to_BuPaIdentification[1] = {};
		// 		to_BuPaIdentification[1].BusinessPartner = BusinessPartner;
		// 		to_BuPaIdentification[1].BPIdentificationType = "Z002";
		// 		to_BuPaIdentification[1].BPIdentificationNumber = data.generalData.grocerWineLicenceNumber;
		// 		to_BuPaIdentification[1].BPIdnNmbrIssuingInstitute = "";
		// 		to_BuPaIdentification[1].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
		// 		// to_BuPaIdentification[1].BPIdentificationEntryDate = new Date();
		// 		// data.grocerWineLicenceNumberStartDate
		// 		// var wineLicenceStartDate = dateFormat.format(new Date()) + "T00:00:00";
		// 		to_BuPaIdentification[1].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //wineLicenceStartDate; //new Date(wineLicenceStartDate); //oDateFormat.format(data.generalData.grocerWineLicenceNumberStartDate);
		// 		to_BuPaIdentification[1].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.grocerWineLicenceNumberEndDate;
		// 		to_BuPaIdentification[1].Country = "";
		// 		to_BuPaIdentification[1].Region = "";
		// 		to_BuPaIdentification[1].AuthorizationGroup = "";
		// 	}
		// 	//*********Distribution License
		// 	if (data.generalData.distributionLicencNumber) {
		// 		to_BuPaIdentification[2] = {};
		// 		to_BuPaIdentification[2].BusinessPartner = BusinessPartner;
		// 		to_BuPaIdentification[2].BPIdentificationType = "Z003";
		// 		to_BuPaIdentification[2].BPIdentificationNumber = data.generalData.distributionLicencNumber;
		// 		to_BuPaIdentification[2].BPIdnNmbrIssuingInstitute = "";
		// 		to_BuPaIdentification[2].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
		// 		// // to_BuPaIdentification[2].BPIdentificationEntryDate = new Date();
		// 		to_BuPaIdentification[2].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberStartDate;
		// 		to_BuPaIdentification[2].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberEndDate;
		// 		to_BuPaIdentification[2].Country = "";
		// 		to_BuPaIdentification[2].Region = "";
		// 		to_BuPaIdentification[2].AuthorizationGroup = "";
		// 	}

		// 	// if (data.)
		// 	///**************Business Partner Role - to_BusinessPartnerRole*************////
		// 	////_______________________________________________________________________/////
		// 	// var to_BusinessPartnerRole = this._businessPartnerRole(BusinessPartner);

		// 	///**************Business Partner Contact - to_BusinessPartnerContact*************////
		// 	////_______________________________________________________________________/////
		// 	// var to_BusinessPartnerContact = [];

		// 	///**************Business Partner Tax - to_BusinessPartnerTax*************////
		// 	////_______________________________________________________________________/////			
		// 	var to_BusinessPartnerTax = [];
		// 	var bpTaxCount = 0;
		// 	if (data.generalData.vatRegistrationNo) {
		// 		// bpTaxCount++;
		// 		to_BusinessPartnerTax[bpTaxCount] = {};
		// 		to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
		// 		to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA1";
		// 		to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = data.generalData.vatRegistrationNo;
		// 	}
		// 	if (data.generalData.entityRegistrationNumber) {
		// 		bpTaxCount++;
		// 		to_BusinessPartnerTax[bpTaxCount] = {};
		// 		to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
		// 		to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA4";
		// 		to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = data.generalData.entityRegistrationNumber;
		// 	}
		// 	////------------Customer Object - to_Customer--------------------////
		// 	////_______________Customer Company Variable______________//////
		// 	// debugger;
		// 	var to_CustomerCompany = [];
		// 	var to_CustomerSalesArea = [];
		// 	var Company = data.company;
		// 	if (Company) {
		// 		for (var cCount = 0; cCount < Company.length; cCount++) {
		// 			////*********Coding to create the company
		// 			to_CustomerCompany[cCount] = {};
		// 			to_CustomerCompany[cCount].Customer = BusinessPartner;
		// 			to_CustomerCompany[cCount].CompanyCode = Company[cCount].companyCode;
		// 			to_CustomerCompany[cCount].AccountingClerkInternetAddress = Company[cCount].accountingClerkEmails;
		// 			to_CustomerCompany[cCount].CustomerAccountNote = "PAYMENT REF STORE";
		// 			to_CustomerCompany[cCount].CustomerSupplierClearingIsUsed = false;
		// 			to_CustomerCompany[cCount].InterestCalculationCode = "Z1";
		// 			to_CustomerCompany[cCount].IntrstCalcFrequencyInMonths = "0";
		// 			to_CustomerCompany[cCount].IsToBeLocallyProcessed = false;
		// 			to_CustomerCompany[cCount].ItemIsToBePaidSeparately = false;
		// 			to_CustomerCompany[cCount].LayoutSortingRule = "001";
		// 			to_CustomerCompany[cCount].PaymentMethodsList = Company[cCount].paymentMethods;
		// 			to_CustomerCompany[cCount].PaymentTerms = Company[cCount].paymentTerms;
		// 			to_CustomerCompany[cCount].PaytAdviceIsSentbyEDI = false;
		// 			to_CustomerCompany[cCount].PhysicalInventoryBlockInd = false;
		// 			to_CustomerCompany[cCount].ReconciliationAccount = Company[cCount].reconciliationAccount;
		// 			to_CustomerCompany[cCount].RecordPaymentHistoryIndicator = true;
		// 			// to_CustomerCompany[cCount].UserAtCustomer = "";
		// 			to_CustomerCompany[cCount].DeletionIndicator = false;

		// 			// if(to_CustomerCompany[cCount].CompanyCode === Managing DC){
		// 			// to_CustomerCompany[cCount].to_CustomerDunning = {};
		// 			// to_CustomerCompany[cCount].to_WithHoldingTax = {};
		// 			// }
		// 			// else{
		// 			to_CustomerCompany[cCount].to_CustomerDunning = [];
		// 			to_CustomerCompany[cCount].to_WithHoldingTax = [];
		// 			// }
		// 			////***********Coding to create the sales area
		// 			to_CustomerSalesArea[cCount] = {};
		// 			to_CustomerSalesArea[cCount].Customer = BusinessPartner;
		// 			to_CustomerSalesArea[cCount].SalesOrganization = Company[cCount].companyCode;
		// 			to_CustomerSalesArea[cCount].DistributionChannel = "95";
		// 			to_CustomerSalesArea[cCount].Division = "01";
		// 			to_CustomerSalesArea[cCount].CompleteDeliveryIsDefined = false;
		// 			to_CustomerSalesArea[cCount].Currency = "ZAR";
		// 			to_CustomerSalesArea[cCount].CustomerPaymentTerms = Company[cCount].paymentTerms;
		// 			to_CustomerSalesArea[cCount].CustomerPricingProcedure = "1";
		// 			to_CustomerSalesArea[cCount].DeliveryPriority = "2";
		// 			to_CustomerSalesArea[cCount].DeletionIndicator = false;
		// 			to_CustomerSalesArea[cCount].ItemOrderProbabilityInPercent = "0";
		// 			to_CustomerSalesArea[cCount].OrderCombinationIsAllowed = true;
		// 			to_CustomerSalesArea[cCount].ShippingCondition = "01";
		// 			///Codeing for the partner function in sales area
		// 			to_CustomerSalesArea[cCount].to_PartnerFunction = [];
		// 			for (var sCount = 0; sCount < 4; sCount++) {
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount] = {};
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Customer = BusinessPartner;
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].SalesOrganization = Company[cCount].companyCode;
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DistributionChannel = "95";
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Division = "01";
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].PartnerCounter = "0";
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].BPCustomerNumber = BusinessPartner;
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].CustomerPartnerDescription = "";
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DefaultPartner = false;
		// 				to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].AuthorizationGroup = "";
		// 			}
		// 			to_CustomerSalesArea[cCount].to_PartnerFunction[0].PartnerFunction = "SP";
		// 			to_CustomerSalesArea[cCount].to_PartnerFunction[1].PartnerFunction = "BP";
		// 			to_CustomerSalesArea[cCount].to_PartnerFunction[2].PartnerFunction = "PY";
		// 			to_CustomerSalesArea[cCount].to_PartnerFunction[3].PartnerFunction = "SH";

		// 			//Codeing for the sales tax area in sales area	
		// 			to_CustomerSalesArea[cCount].to_SalesAreaTax = [];
		// 			for (var taxCount = 0; taxCount < 5; taxCount++) {
		// 				to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount] = {};
		// 				to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Customer = BusinessPartner;
		// 				to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].SalesOrganization = Company[cCount].companyCode;
		// 				to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].DistributionChannel = "95";
		// 				to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Division = "01";
		// 				to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].CustomerTaxCategory = "MWST";

		// 			}
		// 			to_CustomerSalesArea[cCount].to_SalesAreaTax[0].DepartureCountry = "SG";
		// 			to_CustomerSalesArea[cCount].to_SalesAreaTax[1].DepartureCountry = "MZ";
		// 			to_CustomerSalesArea[cCount].to_SalesAreaTax[2].DepartureCountry = "NA";
		// 			to_CustomerSalesArea[cCount].to_SalesAreaTax[3].DepartureCountry = "ZA";
		// 			to_CustomerSalesArea[cCount].to_SalesAreaTax[4].DepartureCountry = "BW";
		// 		}
		// 	}
		// 	// var LayoutSortingRule = "001";
		// 	// var RecordPaymentHistoryIndicator = true;
		// 	// "Customer": "80314",
		// 	// "CompanyCode": "1061",
		// 	// "AccountingClerk": "",
		// 	// "AccountByCustomer": "",
		// 	// "LayoutSortingRule": "001",
		// 	// "ReconciliationAccount": "140000",
		// 	// "PaymentTerms": "Z30D",
		// 	// "PaymentMethodsList": "D",
		// 	//"RecordPaymentHistoryIndicator": true,
		// 	// "UserAtCustomer": "CLERK 1",
		// 	// "AccountingClerkInternetAddress": "manager@knowlesops.co.za"
		// 	/////___________________________________________________//////
		// 	//////_______________Customer Sales Area________________/////
		// 	// var CustomerSalesAreaPartnerFunction = [];
		// 	// var CustomerSalesAreaTax = [];
		// 	// var CustomerSalesArea = [];
		// 	//////__________________________________________________//////
		// 	var to_Customer = {};
		// 	to_Customer.Customer = BusinessPartner;
		// 	to_Customer.DeletionIndicator = false;
		// 	to_Customer.CustomerAccountGroup = data.generalData.group;
		// 	to_Customer.CustomerFullName = OrganizationBPName1;
		// 	to_Customer.CustomerName = OrganizationBPName1;
		// 	to_Customer.PostingIsBlocked = false;
		// 	to_Customer.InternationalLocationNumber1 = "0";
		// 	to_Customer.TaxNumber1 = data.generalData.vatRegistrationNo;
		// 	to_Customer.TaxNumber3 = data.generalData.entityRegistrationNumber;
		// 	to_Customer.DeletionIndicator = false;
		// 	to_Customer.to_CustomerCompany = to_CustomerCompany;
		// 	to_Customer.to_CustomerSalesArea = [];
		// 	// to_CustomerSalesArea;

		// 	var request = {
		// 		BusinessPartner: BusinessPartner,
		// 		Customer: BusinessPartner,
		// 		Supplier: BusinessPartner,
		// 		BusinessPartnerCategory: BusinessPartnerCategory,
		// 		BusinessPartnerFullName: OrganizationBPName1,
		// 		BusinessPartnerGrouping: BusinessPartnerGrouping,
		// 		BusinessPartnerName: OrganizationBPName1,
		// 		InternationalLocationNumber1: "0",
		// 		InternationalLocationNumber2: "0",
		// 		IsFemale: false,
		// 		IsMale: false,
		// 		IsSexUnknown: false,
		// 		OrganizationBPName1: OrganizationBPName1,
		// 		OrganizationBPName4: OrganizationBPName1,
		// 		SearchTerm1: SearchTerm1,
		// 		BusinessPartnerIsBlocked: BusinessPartnerIsBlocked,
		// 		BusinessPartnerType: "0002",
		// 		InternationalLocationNumber3: "0",
		// 		IsMarkedForArchiving: false,
		// 		to_BuPaIdentification: to_BuPaIdentification,

		// 		to_BusinessPartnerAddress: [{
		// 			BusinessPartner: BusinessPartner,
		// 			CityName: citySAddress,
		// 			Country: countrySAddress,
		// 			District: subuRbSAddress,
		// 			FullName: OrganizationBPName1,
		// 			HouseNumber: complexNumberSAddress,
		// 			Language: "EN",
		// 			PostalCode: postalCodeSAddress,
		// 			PrfrdCommMediumType: PrfrdCommMediumType,
		// 			Region: provinceSAddres,
		// 			StreetName: streetNameSAddress,
		// 			StreetPrefixName: StreetPrefixName,
		// 			////**********Postal Address*******///
		// 			DeliveryServiceNumber: addressNumberPAddress,
		// 			DeliveryServiceTypeCode: "", //addressTypePAddress,
		// 			POBoxPostalCode: postalCodePAddress,
		// 			POBoxDeviatingCityName: cityPAddress,
		// 			POBoxDeviatingRegion: provincePAddress,
		// 			POBoxDeviatingCountry: countryPAddress,
		// 			to_AddressUsage: [{
		// 				BusinessPartner: BusinessPartner,
		// 				//ValidityEndDate:"9999-01-01T00:00:00Z",
		// 				AddressUsage: AddressUsage
		// 					//ValidityStartDate":"2018-09-21T15:20:15Z"
		// 			}],

		// 			to_PhoneNumber: to_PhoneNumber,
		// 			to_MobilePhoneNumber: to_MobilePhoneNumber,
		// 			to_FaxNumber: to_FaxNumber,
		// 			to_EmailAddress: to_EmailAddress
		// 		}],
		// 		to_BusinessPartnerBank: to_BusinessPartnerBank,
		// 		// var to_BusinessPartnerRole = ;
		// 		to_BusinessPartnerRole: this._businessPartnerRole(BusinessPartner),
		// 		// to_BusinessPartnerContact: to_BusinessPartnerContact,
		// 		to_BusinessPartnerTax: to_BusinessPartnerTax,
		// 		to_Customer: to_Customer
		// 	};
		// 	return request;
		// },
		buildServiceObjectMapFromView_Update: function (viewData, orgData) {

			var respData = {};
			respData.generalData = {};
			respData.contacts = {};
			respData.address = {};
			respData.bpData = [];
			///********************************************
			///converting mandatory fields to upper case
			viewData.generalData.nameGroupDetail.toUpperCase();
			viewData.generalData.entityRegistrationNumber.toUpperCase();
			viewData.address.streetNameSAddress.toUpperCase();
			viewData.address.citySAddress.toUpperCase();
			viewData.address.postalCodeSAddress.toUpperCase();
			///********************************************
			var BusinessPartner = viewData.generalData.code;

			////////________________________________________________//////
			var OrganizationBPName1 = (orgData.OrganizationBPName1 ? ((orgData.OrganizationBPName1 === viewData.generalData.nameGroupDetail) ?
				orgData.OrganizationBPName1 : viewData.generalData.nameGroupDetail) : viewData.generalData.nameGroupDetail);
			var SearchTerm1 = (orgData.SearchTerm1 ? ((orgData.SearchTerm1 === viewData.generalData.nameGroupDetail) ?
				orgData.SearchTerm1 : viewData.generalData.searchTerm1) : viewData.generalData.searchTerm1);

			// var BusinessPartnerIsBlocked = false;
			// var BusinessPartnerCategory = "2";
			var BusinessPartnerGrouping = (orgData.BusinessPartnerGrouping ? ((orgData.BusinessPartnerGrouping === viewData.generalData.group) ?
				orgData.BusinessPartnerGrouping : viewData.generalData.group) : viewData.generalData.group);

			////**********************Start of Address viewData**************/////

			var complexNumberSAddress =
				function () {
					if (viewData.address.complexNumberSAddress)
						return viewData.address.complexNumberSAddress;
					else return "";
				};
			var streetNumberSAddress = viewData.address.streetNumberSAddress;

			var streetNameSAddress = (orgData.to_BusinessPartnerAddress.StreetName ? ((orgData.to_BusinessPartnerAddress.StreetName === viewData.address
					.streetNameSAddress) ?
				orgData.to_BusinessPartnerAddress.StreetName : viewData.address.streetNameSAddress) : viewData.address.streetNameSAddress);

			var subuRbSAddress = (orgData.to_BusinessPartnerAddress.District ? ((orgData.to_BusinessPartnerAddress.District === viewData.address.subuRbSAddress) ?
				orgData.to_BusinessPartnerAddress.District : viewData.address.subuRbSAddress) : viewData.address.subuRbSAddress);

			// viewData.address.subuRbSAddress;
			var citySAddress = (orgData.to_BusinessPartnerAddress.CityName ? ((orgData.to_BusinessPartnerAddress.CityName === viewData.address.citySAddress) ?
				orgData.to_BusinessPartnerAddress.CityName : viewData.address.citySAddress) : viewData.address.citySAddress);

			var postalCodeSAddress = (orgData.to_BusinessPartnerAddress.PostalCode ? ((orgData.to_BusinessPartnerAddress.PostalCode === viewData.address
					.postalCodeSAddress) ?
				orgData.to_BusinessPartnerAddress.PostalCode : viewData.address.postalCodeSAddress) : viewData.address.postalCodeSAddress);

			var provinceSAddres = (orgData.to_BusinessPartnerAddress.Region ? ((orgData.to_BusinessPartnerAddress.Region === viewData.address.provinceSAddres) ?
				orgData.to_BusinessPartnerAddress.Region : viewData.address.provinceSAddres) : viewData.address.provinceSAddres);

			var countrySAddress = (orgData.to_BusinessPartnerAddress.Country ? ((orgData.to_BusinessPartnerAddress.Country === viewData.address.countrySAddress) ?
				orgData.to_BusinessPartnerAddress.Country : viewData.address.countrySAddress) : viewData.address.countrySAddress);

			var PrfrdCommMediumType = (orgData.to_BusinessPartnerAddress.PrfrdCommMediumType ? ((orgData.to_BusinessPartnerAddress.PrfrdCommMediumType ===
					"INT") ?
				orgData.to_BusinessPartnerAddress.PrfrdCommMediumType : "INT") : "INT");
			var StreetPrefixName = (orgData.to_BusinessPartnerAddress.StreetPrefixName ? ((orgData.to_BusinessPartnerAddress.StreetPrefixName ===
					"") ?
				orgData.to_BusinessPartnerAddress.StreetPrefixName : "") : "");

			var addressTypePAddress = viewData.address.addressTypePAddress;

			var addressNumberPAddress = (orgData.to_BusinessPartnerAddress.DeliveryServiceNumber ? ((orgData.to_BusinessPartnerAddress.DeliveryServiceNumber ===
					viewData.address
					.addressNumberPAddress) ?
				orgData.to_BusinessPartnerAddress.DeliveryServiceNumber : viewData.address.addressNumberPAddress) : viewData.address.addressNumberPAddress);

			var cityPAddress = (orgData.to_BusinessPartnerAddress.POBoxDeviatingCityName ? ((orgData.to_BusinessPartnerAddress.POBoxDeviatingCityName ===
					viewData.address.cityPAddress) ?
				orgData.to_BusinessPartnerAddress.POBoxDeviatingCityName : viewData.address.cityPAddress) : viewData.address.cityPAddress);

			var postalCodePAddress = (orgData.to_BusinessPartnerAddress.POBoxPostalCode ? ((orgData.to_BusinessPartnerAddress.POBoxPostalCode ===
					viewData.address.postalCodePAddress) ?
				orgData.to_BusinessPartnerAddress.POBoxPostalCode : viewData.address.postalCodePAddress) : viewData.address.postalCodePAddress);

			var provincePAddress = (orgData.to_BusinessPartnerAddress.POBoxDeviatingRegion ? ((orgData.to_BusinessPartnerAddress.POBoxDeviatingRegion ===
					viewData.address.provincePAddress) ?
				orgData.to_BusinessPartnerAddress.POBoxDeviatingRegion : viewData.address.provincePAddress) : viewData.address.provincePAddress);

			var countryPAddress = (orgData.to_BusinessPartnerAddress.POBoxDeviatingCountry ? ((orgData.to_BusinessPartnerAddress.POBoxDeviatingCountry ===
					viewData.address.countryPAddress) ?
				orgData.to_BusinessPartnerAddress.POBoxDeviatingCountry : viewData.address.countryPAddress) : viewData.address.countryPAddress);

			var AddressUsage = "XXDEFAULT";
			//////*********************End of Address Data*****************/////

			//////*********************Start of Contact Data*****************/////

			///**********Creating Telephone Object-to_PhoneNumber***********///

			// var telephoneArray = [];

			var to_PhoneNumber = [];
			var to_FaxNumber = [];
			var to_EmailAddress = [];

			if (viewData.contacts) {
				var iPhone = 0;
				var telephoneArray = {};
				if (viewData.contacts.telephoneData) {
					telephoneArray = viewData.contacts.telephoneData;
					for (; iPhone < telephoneArray.length; iPhone++) {
						to_PhoneNumber[iPhone] = {};
						to_PhoneNumber[iPhone].PhoneNumberType = "1";
						to_PhoneNumber[iPhone].IsDefaultPhoneNumber = telephoneArray[iPhone].primary;
						to_PhoneNumber[iPhone].PhoneNumber = telephoneArray[iPhone].telephoneNo;
						to_PhoneNumber[iPhone].Person = telephoneArray[iPhone].telephoneName;
						to_PhoneNumber[iPhone].PhoneNumberExtension = telephoneArray[iPhone].telephoneExtension;
						to_PhoneNumber[iPhone].InternationalPhoneNumber = "+" + telephoneArray[iPhone].telephoneDialingCode + "" + telephoneArray[iPhone]
							.telephoneNo;
						to_PhoneNumber[iPhone].PhoneNumberType = telephoneArray[iPhone].idTelephoneTabContactType;
						to_PhoneNumber[iPhone].OrdinalNumber = iPhone.toString();
					}
				}
				///**********Creating Cellphone Object-to_PhoneNumber***********///
				////------------------------------------------------------------////
				if (viewData.contacts.cellphoneData) {
					var cellphoneArray = viewData.contacts.cellphoneData;
					var cPhone = 0;
					var totalLength = 0;
					// if (telephoneArray) {
					totalLength = telephoneArray.length + cellphoneArray.length;
					// } else {
					// 	totalLength = cellphoneArray.length;
					// }

					for (; iPhone < totalLength; iPhone++) {
						to_PhoneNumber[iPhone] = {};
						to_PhoneNumber[iPhone].PhoneNumberType = "3";
						to_PhoneNumber[iPhone].IsDefaultPhoneNumber = cellphoneArray[cPhone].primary;
						to_PhoneNumber[iPhone].Person = cellphoneArray[cPhone].cellphoneName;
						to_PhoneNumber[iPhone].PhoneNumber = cellphoneArray[cPhone].cellphoneNo;
						to_PhoneNumber[iPhone].InternationalPhoneNumber = "+" + cellphoneArray[cPhone].cellphoneDialingCode + "" + cellphoneArray[cPhone]
							.cellphoneNo;
						to_PhoneNumber[iPhone].PhoneNumberType = cellphoneArray[cPhone].cellphoneContactType;
						to_PhoneNumber[iPhone].OrdinalNumber = iPhone.toString();
					}
				}
				///**********Creating Fax Object-to_FaxNumber***********///
				////------------------------------------------------------------////
				if (viewData.contacts.faxData) {
					var faxArray = viewData.contacts.faxData;
					for (var iFax = 0; iFax < faxArray.length; iFax++) {
						to_FaxNumber[iFax] = {};
						to_FaxNumber[iFax].IsDefaultFaxNumber = faxArray[iFax].primary;
						to_FaxNumber[iFax].FaxNumber = faxArray[iFax].faxNo;
						to_FaxNumber[iFax].Person = faxArray[iFax].faxName;
						to_FaxNumber[iFax].InternationalFaxNumber = "+" + faxArray[iFax].faxDialingCode + "" + faxArray[iFax].faxNo;
						to_FaxNumber[iFax].OrdinalNumber = iFax.toString();
					}
				}
				///**********Creating Eail Object-to_EmailAddress***********///
				////------------------------------------------------------------////
				if (viewData.contacts.emailData) {
					var emailArray = viewData.contacts.emailData;
					for (var iEmail = 0; iEmail < emailArray.length; iEmail++) {
						to_EmailAddress[iEmail] = {};
						to_EmailAddress[iEmail].IsDefaultEmailAddress = emailArray[iEmail].primary;
						to_EmailAddress[iEmail].Person = emailArray[iEmail].emailName;
						to_EmailAddress[iEmail].EmailAddress = emailArray[iEmail].emailId;
						to_EmailAddress[iEmail].SearchEmailAddress = emailArray[iEmail].emailId;
						to_EmailAddress[iEmail].OrdinalNumber = iEmail.toString();
					}
				}
			}
			///////*****************************Bank Detail Section**********************///
			/////___________________________________________________________________///////

			var to_BusinessPartnerBank = [];
			var bankData = viewData.bankData;
			if (bankData) {
				for (var iBank = 0; iBank < bankData.length; iBank++) {
					to_BusinessPartnerBank[iBank] = {};
					to_BusinessPartnerBank[iBank].BusinessPartner = BusinessPartner;
					var bankId = iBank.toString();
					if (bankId.length === 1) {
						bankId = "000" + bankId;
					} else if (bankId.length === 2) {
						bankId = "00" + bankId;
					}
					to_BusinessPartnerBank[iBank].BankIdentification = bankId;
					to_BusinessPartnerBank[iBank].BankCountryKey = bankData[iBank].countryBankDetail;
					to_BusinessPartnerBank[iBank].BankName = bankData[iBank].bankNameBank;
					to_BusinessPartnerBank[iBank].BankNumber = bankData[iBank].bankCodeBank;
					to_BusinessPartnerBank[iBank].BankAccountHolderName = bankData[iBank].accountNameBank;
					to_BusinessPartnerBank[iBank].BankAccount = bankData[iBank].accountNumberBank;
					to_BusinessPartnerBank[iBank].CollectionAuthInd = true;
				}
			}
			///**************Business Partner Identification - to_BuPaIdentification************////
			////_______________________________________________________________________________/////

			var to_BuPaIdentification = this._buPaIdentification(viewData, orgData);

			///**************Business Partner Role - to_BusinessPartnerRole*************////
			////_______________________________________________________________________/////
			var to_BusinessPartnerRole = orgData.to_BusinessPartnerRole ? orgData.to_BusinessPartnerRole : this._businessPartnerRole(
				BusinessPartner);

			///**************Business Partner Contact - to_BusinessPartnerContact*************////
			////_______________________________________________________________________/////
			// var to_BusinessPartnerContact = [];

			///**************Business Partner Tax - to_BusinessPartnerTax*************////
			////_______________________________________________________________________/////			
			var to_BusinessPartnerTax = [];
			var bpTaxCount = 0;

			if (viewData.generalData.vatRegistrationNo) {
				to_BusinessPartnerTax[bpTaxCount] = {};
				to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
				to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA1";
				to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = viewData.generalData.vatRegistrationNo;
			}
			if (viewData.generalData.entityRegistrationNumber) {
				bpTaxCount++;
				to_BusinessPartnerTax[bpTaxCount] = {};
				to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
				to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA4";
				to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = viewData.generalData.entityRegistrationNumber;
			}
			////------------Customer Object - to_Customer--------------------////
			////_______________Customer Company Variable______________//////
			// debugger;
			var to_CustomerCompany = [];
			var to_CustomerSalesArea = [];
			var Company = viewData.company;
			if (Company) {
				for (var cCount = 0; cCount < Company.length; cCount++) {
					////*********Coding to create the company
					to_CustomerCompany[cCount] = {};
					to_CustomerCompany[cCount].Customer = BusinessPartner;
					to_CustomerCompany[cCount].CompanyCode = Company[cCount].companyCode;
					to_CustomerCompany[cCount].AccountingClerkInternetAddress = Company[cCount].accountingClerkEmails;
					to_CustomerCompany[cCount].CustomerAccountNote = "PAYMENT REF STORE";
					to_CustomerCompany[cCount].CustomerSupplierClearingIsUsed = false;
					to_CustomerCompany[cCount].InterestCalculationCode = "Z1";
					to_CustomerCompany[cCount].IntrstCalcFrequencyInMonths = "0";
					to_CustomerCompany[cCount].IsToBeLocallyProcessed = false;
					to_CustomerCompany[cCount].ItemIsToBePaidSeparately = false;
					to_CustomerCompany[cCount].LayoutSortingRule = "001";
					to_CustomerCompany[cCount].PaymentMethodsList = Company[cCount].paymentMethods;
					to_CustomerCompany[cCount].PaymentTerms = Company[cCount].paymentTerms;
					to_CustomerCompany[cCount].PaytAdviceIsSentbyEDI = false;
					to_CustomerCompany[cCount].PhysicalInventoryBlockInd = false;
					to_CustomerCompany[cCount].ReconciliationAccount = Company[cCount].reconciliationAccount;
					to_CustomerCompany[cCount].RecordPaymentHistoryIndicator = true;
					// to_CustomerCompany[cCount].UserAtCustomer = "";
					to_CustomerCompany[cCount].DeletionIndicator = false;

					// if(to_CustomerCompany[cCount].CompanyCode === Managing DC){
					// to_CustomerCompany[cCount].to_CustomerDunning = {};
					// to_CustomerCompany[cCount].to_WithHoldingTax = {};
					// }
					// else{
					to_CustomerCompany[cCount].to_CustomerDunning = [];
					to_CustomerCompany[cCount].to_WithHoldingTax = [];
					// }
					////***********Coding to create the sales area
					to_CustomerSalesArea[cCount] = {};
					to_CustomerSalesArea[cCount].Customer = BusinessPartner;
					to_CustomerSalesArea[cCount].SalesOrganization = Company[cCount].companyCode;
					to_CustomerSalesArea[cCount].DistributionChannel = "95";
					to_CustomerSalesArea[cCount].Division = "01";
					to_CustomerSalesArea[cCount].CompleteDeliveryIsDefined = false;
					to_CustomerSalesArea[cCount].Currency = "ZAR";
					to_CustomerSalesArea[cCount].CustomerPaymentTerms = Company[cCount].paymentTerms;
					to_CustomerSalesArea[cCount].CustomerPricingProcedure = "1";
					to_CustomerSalesArea[cCount].DeliveryPriority = "2";
					to_CustomerSalesArea[cCount].DeletionIndicator = false;
					to_CustomerSalesArea[cCount].ItemOrderProbabilityInPercent = "0";
					to_CustomerSalesArea[cCount].OrderCombinationIsAllowed = true;
					to_CustomerSalesArea[cCount].ShippingCondition = "01";
					///Codeing for the partner function in sales area
					to_CustomerSalesArea[cCount].to_PartnerFunction = [];
					for (var sCount = 0; sCount < 4; sCount++) {
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount] = {};
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Customer = BusinessPartner;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].SalesOrganization = Company[cCount].companyCode;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DistributionChannel = "95";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Division = "01";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].PartnerCounter = "0";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].BPCustomerNumber = BusinessPartner;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].CustomerPartnerDescription = "";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DefaultPartner = false;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].AuthorizationGroup = "";
					}
					to_CustomerSalesArea[cCount].to_PartnerFunction[0].PartnerFunction = "SP";
					to_CustomerSalesArea[cCount].to_PartnerFunction[1].PartnerFunction = "BP";
					to_CustomerSalesArea[cCount].to_PartnerFunction[2].PartnerFunction = "PY";
					to_CustomerSalesArea[cCount].to_PartnerFunction[3].PartnerFunction = "SH";

					//Codeing for the sales tax area in sales area	
					to_CustomerSalesArea[cCount].to_SalesAreaTax = [];
					for (var taxCount = 0; taxCount < 5; taxCount++) {
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount] = {};
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Customer = BusinessPartner;
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].SalesOrganization = Company[cCount].companyCode;
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].DistributionChannel = "95";
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Division = "01";
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].CustomerTaxCategory = "MWST";

					}
					to_CustomerSalesArea[cCount].to_SalesAreaTax[0].DepartureCountry = "SG";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[1].DepartureCountry = "MZ";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[2].DepartureCountry = "NA";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[3].DepartureCountry = "ZA";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[4].DepartureCountry = "BW";
				}
			}
			// var LayoutSortingRule = "001";
			// var RecordPaymentHistoryIndicator = true;
			// "Customer": "80314",
			// "CompanyCode": "1061",
			// "AccountingClerk": "",
			// "AccountByCustomer": "",
			// "LayoutSortingRule": "001",
			// "ReconciliationAccount": "140000",
			// "PaymentTerms": "Z30D",
			// "PaymentMethodsList": "D",
			//"RecordPaymentHistoryIndicator": true,
			// "UserAtCustomer": "CLERK 1",
			// "AccountingClerkInternetAddress": "manager@knowlesops.co.za"
			/////___________________________________________________//////
			//////_______________Customer Sales Area________________/////
			// var CustomerSalesAreaPartnerFunction = [];
			// var CustomerSalesAreaTax = [];
			// var CustomerSalesArea = [];
			//////__________________________________________________//////
			var to_Customer = {};
			to_Customer.Customer = BusinessPartner;
			to_Customer.DeletionIndicator = false;
			to_Customer.CustomerAccountGroup = viewData.generalData.group;
			to_Customer.CustomerFullName = OrganizationBPName1;
			to_Customer.CustomerName = OrganizationBPName1;
			to_Customer.PostingIsBlocked = false;
			to_Customer.InternationalLocationNumber1 = "0";
			to_Customer.TaxNumber1 = viewData.generalData.vatRegistrationNo;
			to_Customer.TaxNumber3 = viewData.generalData.entityRegistrationNumber;
			to_Customer.DeletionIndicator = false;
			to_Customer.to_CustomerCompany = to_CustomerCompany;
			to_Customer.to_CustomerSalesArea = [];
			// to_CustomerSalesArea;
			// debugger;
			var request = {
				BusinessPartner: BusinessPartner,
				Customer: BusinessPartner,
				Supplier: BusinessPartner,
				// BusinessPartnerCategory: BusinessPartnerCategory,

				BusinessPartnerFullName: OrganizationBPName1,
				BusinessPartnerGrouping: BusinessPartnerGrouping,
				BusinessPartnerName: OrganizationBPName1,
				InternationalLocationNumber1: "0",
				InternationalLocationNumber2: "0",
				IsFemale: false,
				IsMale: false,
				IsSexUnknown: false,
				OrganizationBPName1: OrganizationBPName1,
				OrganizationBPName4: OrganizationBPName1,
				SearchTerm1: SearchTerm1,
				// BusinessPartnerIsBlocked: BusinessPartnerIsBlocked,
				BusinessPartnerType: "0002",
				InternationalLocationNumber3: "0",
				IsMarkedForArchiving: false,
				to_BuPaIdentification: to_BuPaIdentification,

				to_BusinessPartnerAddress: [{
					BusinessPartner: BusinessPartner,
					CityName: citySAddress,
					Country: countrySAddress,
					District: subuRbSAddress,
					FullName: OrganizationBPName1,
					HouseNumber: complexNumberSAddress,
					Language: "EN",
					PostalCode: postalCodeSAddress,
					PrfrdCommMediumType: PrfrdCommMediumType,
					Region: provinceSAddres,
					StreetName: streetNameSAddress,
					StreetPrefixName: StreetPrefixName,
					////**********Postal Address*******///
					DeliveryServiceNumber: addressNumberPAddress,
					DeliveryServiceTypeCode: "", //addressTypePAddress,
					POBoxPostalCode: postalCodePAddress,
					POBoxDeviatingCityName: cityPAddress,
					POBoxDeviatingRegion: provincePAddress,
					POBoxDeviatingCountry: countryPAddress,
					to_AddressUsage: [{
						BusinessPartner: BusinessPartner,
						//ValidityEndDate:"9999-01-01T00:00:00Z",
						AddressUsage: AddressUsage
							//ValidityStartDate":"2018-09-21T15:20:15Z"
					}],

					to_PhoneNumber: to_PhoneNumber,
					to_FaxNumber: to_FaxNumber,
					to_EmailAddress: to_EmailAddress
				}],
				to_BusinessPartnerBank: to_BusinessPartnerBank,
				// var to_BusinessPartnerRole = ;
				to_BusinessPartnerRole: this._businessPartnerRole(BusinessPartner),
				// to_BusinessPartnerContact: to_BusinessPartnerContact,
				to_BusinessPartnerTax: to_BusinessPartnerTax,
				to_Customer: to_Customer
			};
			return request;
		},
		_buPaIdentification: function (data) {
			var to_BuPaIdentification = {},
				BusinessPartner = data.generalData.code;

			//*********Store Liquor License
			if (data.generalData.liquorLicenceNumber) {
				to_BuPaIdentification[0] = {};
				to_BuPaIdentification[0].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[0].BPIdentificationType = "Z001";
				to_BuPaIdentification[0].BPIdentificationNumber = data.generalData.liquorLicenceNumber;
				to_BuPaIdentification[0].BPIdnNmbrIssuingInstitute = "";

				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "yyyy-MM-dd"
				});
				var liquorLicenceEntryDate = dateFormat.format(new Date());
				liquorLicenceEntryDate = liquorLicenceEntryDate + "T00:00:00";
				to_BuPaIdentification[0].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceEntryDate; //new Date("24.05.2020");
				// debugger;
				// var vStartDate = data.liquorLicenceNumberStartDate.getDate() + "." + data.liquorLicenceNumberStartDate.getMonth() + "." + data.generalData.liquorLicenceNumberStartDate
				// 	.getFullYear();
				var liquorLicenceStartDate = dateFormat.format(data.generalData.liquorLicenceNumberStartDate);
				liquorLicenceStartDate = liquorLicenceStartDate + "T00:00:00";
				to_BuPaIdentification[0].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceStartDate; //new Date(liquorLicenceStartDate); //data.generalData.liquorLicenceNumberStartDate;

				to_BuPaIdentification[0].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //new Date("17.05.2020"); //data.generalData.liquorLicenceNumberEndDate;
				to_BuPaIdentification[0].Country = "";
				to_BuPaIdentification[0].Region = "";
				to_BuPaIdentification[0].AuthorizationGroup = "";
			}
			//*********Grocer's Wine License
			if (data.generalData.grocerWineLicenceNumber) {
				to_BuPaIdentification[1] = {};
				to_BuPaIdentification[1].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[1].BPIdentificationType = "Z002";
				to_BuPaIdentification[1].BPIdentificationNumber = data.generalData.grocerWineLicenceNumber;
				to_BuPaIdentification[1].BPIdnNmbrIssuingInstitute = "";
				to_BuPaIdentification[1].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
				// to_BuPaIdentification[1].BPIdentificationEntryDate = new Date();
				// data.grocerWineLicenceNumberStartDate
				// var wineLicenceStartDate = dateFormat.format(new Date()) + "T00:00:00";
				to_BuPaIdentification[1].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //wineLicenceStartDate; //new Date(wineLicenceStartDate); //oDateFormat.format(data.generalData.grocerWineLicenceNumberStartDate);
				to_BuPaIdentification[1].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.grocerWineLicenceNumberEndDate;
				to_BuPaIdentification[1].Country = "";
				to_BuPaIdentification[1].Region = "";
				to_BuPaIdentification[1].AuthorizationGroup = "";
			}
			//*********Distribution License
			if (data.generalData.distributionLicencNumber) {
				to_BuPaIdentification[2] = {};
				to_BuPaIdentification[2].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[2].BPIdentificationType = "Z003";
				to_BuPaIdentification[2].BPIdentificationNumber = data.generalData.distributionLicencNumber;
				to_BuPaIdentification[2].BPIdnNmbrIssuingInstitute = "";
				to_BuPaIdentification[2].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
				// // to_BuPaIdentification[2].BPIdentificationEntryDate = new Date();
				to_BuPaIdentification[2].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberStartDate;
				to_BuPaIdentification[2].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberEndDate;
				to_BuPaIdentification[2].Country = "";
				to_BuPaIdentification[2].Region = "";
				to_BuPaIdentification[2].AuthorizationGroup = "";
			}
			return to_BuPaIdentification;
		},
		buildingBPViewFromService: function (data, that) {
			// debugger;
			var bpData = {};
			bpData.address = {};
			bpData.contacts = {};
			bpData.generalData = {};

			bpData.contacts.telephoneData = [];
			bpData.contacts.cellphoneData = [];
			bpData.contacts.faxData = [];
			bpData.contacts.emailData = [];

			bpData.bankData = [];
			bpData.company = [];
			try {
				bpData.generalData.code = data.BusinessPartner;
				bpData.generalData.searchTerm1 = this._ifNotThen(data.SearchTerm1, "");

				bpData.generalData.nameGroupDetail = data.OrganizationBPName1;
				bpData.generalData.countryGroupDetail = data.to_BusinessPartnerAddress.results[0] ? (data.to_BusinessPartnerAddress.results[0].Country ?
					data.to_BusinessPartnerAddress.results[0].Country : (data.to_BusinessPartnerAddress.results[0].POBoxDeviatingCountry ? data.to_BusinessPartnerAddress
						.results[0].POBoxDeviatingCountry : "")) : "";
			} catch (error) {
				Log.info("Error occured in mapping general from request to view, Error: ${err}");
			}
			/////******VAT no***********///
			try {
				for (var i = 0; i < data.to_BusinessPartnerTax.results.length; i++) {
					if (data.to_BusinessPartnerTax.results[i].BPTaxType === "ZA1") {
						bpData.generalData.vatRegistrationNo = this._ifNotThen(data.to_BusinessPartnerTax.results[i].BPTaxNumber, "");
					} else if (data.to_BusinessPartnerTax.results[i].BPTaxType === "ZA4") {
						bpData.generalData.entityRegistrationNumber = this._ifNotThen(data.to_BusinessPartnerTax.results[i].BPTaxNumber, "");
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping VAT no from request to view, Error: ${err}");
			}

			///********Licence no********//
			try {
				for (i = 0; i < data.to_BuPaIdentification.results.length; i++) {
					if (data.to_BuPaIdentification.results[i].BPIdentificationType === "Z001") {
						bpData.generalData.liquorLicenceNumber = this._ifNotThen(data.to_BuPaIdentification.results[i].BPIdentificationNumber, "");
						bpData.generalData.liquorLicenceNumberStartDate = this._ifNotThen(new Date(parseInt((data.to_BuPaIdentification.results[i].ValidityStartDate)
							.substr(6))), new Date());
						bpData.generalData.liquorLicenceNumberEndDate = this._ifNotThen(new Date(parseInt((data.to_BuPaIdentification.results[i].ValidityEndDate)
							.substr(6))), new Date());
					} else if (data.to_BuPaIdentification.results[i].BPIdentificationType === "Z002") {
						bpData.generalData.grocerWineLicenceNumber = this._ifNotThen(data.to_BuPaIdentification.results[i].BPIdentificationNumber, "");
						bpData.generalData.grocerWineLicenceNumberStartDate = this._ifNotThen(new Date(parseInt((data.to_BuPaIdentification.results[i].ValidityStartDate)
							.substr(6))), new Date());
						bpData.generalData.grocerWineLicenceNumberEndDate = this._ifNotThen(new Date(parseInt((data.to_BuPaIdentification.results[i].ValidityEndDate)
							.substr(6))), new Date());
					} else if (data.to_BuPaIdentification.results[i].BPIdentificationType === "Z003") {
						bpData.generalData.distributionLicencNumber = this._ifNotThen(data.to_BuPaIdentification.results[i].BPIdentificationNumber, "");
						bpData.generalData.distributionLicencNumberStartDate = this._ifNotThen(new Date(parseInt((data.to_BuPaIdentification.results[i].ValidityStartDate)
							.substr(6))), new Date());
						bpData.generalData.distributionLicencNumberEndDate = this._ifNotThen(new Date(parseInt((data.to_BuPaIdentification.results[i].ValidityEndDate)
							.substr(6))), new Date());
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping Linence no from request to view, Error: ${err}");
			}
			///*************Street Address*****************///
			try {
				var address = data.to_BusinessPartnerAddress.results[0];
				bpData.address.complexNumberSAddress = this._ifNotThen(address.HouseNumber, "");
				bpData.address.streetNameSAddress = this._ifNotThen(address.StreetName, "");
				bpData.address.subuRbSAddress = this._ifNotThen(address.District, "");
				bpData.address.citySAddress = this._ifNotThen(address.CityName, "");
				bpData.address.postalCodeSAddress = this._ifNotThen(address.PostalCode, "");
				bpData.address.provinceSAddres = this._ifNotThen(address.Region, "");
				bpData.address.countrySAddress = this._ifNotThen(address.Country, "");
				bpData.address.addressTypePAddress = this._ifNotThen(address.DeliveryServiceTypeCode, "");
				bpData.address.addressNumberPAddress = this._ifNotThen(address.DeliveryServiceNumber, "");
				bpData.address.cityPAddress = this._ifNotThen(address.POBoxDeviatingCityName, "");
				bpData.address.postalCodePAddress = this._ifNotThen(address.POBoxPostalCode, "");
				bpData.address.provincePAddress = this._ifNotThen(address.POBoxDeviatingRegion, "");
				bpData.address.countryPAddress = this._ifNotThen(address.POBoxDeviatingCountry, "");
			} catch (err) {
				Log.info("Error occured in mapping address from request to view, Error: ${err}");
			}
			////***************Bank****************//////
			try {
				var bank = data.to_BusinessPartnerBank.results;
				if (bank) {
					for (var iBank = 0; iBank < bank.length; iBank++) {
						bpData.bankData[iBank] = {};
						bpData.bankData[iBank].countryBankDetail = bank[iBank].BankCountryKey;

						bpData.bankData[iBank].bankNameBank = bank[iBank].BankName;
						bpData.bankData[iBank].bankCodeBank = bank[iBank].BankNumber;
						bpData.bankData[iBank].accountNumberBank = bank[iBank].BankAccount;
						bpData.bankData[iBank].accountNameBank = bank[iBank].BankAccountHolderName;

						////**********************Bank Validation**************///						
						bpData.bankData[iBank].validFields = {};
						bpData.bankData[iBank].validFields.bankCodeBank = "None";
						bpData.bankData[iBank].validFields.accountNumberBank = "None";
						bpData.bankData[iBank].validFields.accountNameBank = "None";
						bpData.bankData[iBank].validationMsg = {};
						bpData.bankData[iBank].validationMsg.bankCodeBank = "";
						bpData.bankData[iBank].validationMsg.accountNumberBank = "";
						bpData.bankData[iBank].validationMsg.accountNameBank = "";
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping bank detail from request to view, Error: " + error);
			}
			////*****************Company******************////
			try {
				var company = data.to_Customer.to_CustomerCompany.results;
				if (company) {
					for (var iCompany = 0; iCompany < company.length; iCompany++) {
						bpData.company[iCompany] = {};
						bpData.company[iCompany].companyCode = company[iCompany].CompanyCode;
						bpData.company[iCompany].paymentTerms = company[iCompany].PaymentTerms;
						bpData.company[iCompany].paymentMethods = company[iCompany].PaymentMethodsList;
						bpData.company[iCompany].reconciliationAccount = company[iCompany].ReconciliationAccount;
						bpData.company[iCompany].accountingClerkEmails = company[iCompany].AccountingClerkInternetAddress;
						bpData.company[iCompany].userAtCustomer = company[iCompany].UserAtCustomer;

						////**********************Company Validation**************///
						bpData.company[iCompany].validFields = {};
						bpData.company[iCompany].validFields.companyCode = "None";
						bpData.company[iCompany].validFields.userAtCustomer = "None";
						bpData.company[iCompany].validFields.accountingClerkEmails = "None";
						bpData.company[iCompany].validationMsg = {};
						bpData.company[iCompany].validationMsg.companyCode = "";
						bpData.company[iCompany].validationMsg.userAtCustomer = "";
						bpData.company[iCompany].validationMsg.accountingClerkEmails = "";
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping company detail from request to view, Error: " + error);
			}
			///**************************Contact Detail************/////

			///***************telephone***************////
			try {
				var telephone = data.to_BusinessPartnerAddress.results[0].to_PhoneNumber.results;
				if (telephone) {
					// var telephoneData = [];
					for (var iTelephone = 0; iTelephone < telephone.length; iTelephone++) {
						bpData.contacts.telephoneData[iTelephone] = {};
						bpData.contacts.telephoneData[iTelephone].primary = telephone[iTelephone].IsDefaultPhoneNumber;
						var telephoneDialingCode = this._getDialingCode(that, telephone[iTelephone].DestinationLocationCountry);
						bpData.contacts.telephoneData[iTelephone].telephoneDialingCode = telephoneDialingCode;
						bpData.contacts.telephoneData[iTelephone].telephoneNo = telephone[iTelephone].PhoneNumber;
						bpData.contacts.telephoneData[iTelephone].telephoneExtension = telephone[iTelephone].PhoneNumberExtension;

						////**********************Telephone Validation**************///
						bpData.contacts.telephoneData[iTelephone].validFields = {};
						bpData.contacts.telephoneData[iTelephone].validFields.telephoneNo = "None";
						bpData.contacts.telephoneData[iTelephone].validationMsg = {};
						bpData.contacts.telephoneData[iTelephone].validationMsg.telephoneNo = "";
						// }
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping telephone detail from request to view, Error: " + error);
			}
			/////*************Cellphone******************////
			try {
				var cellphone = data.to_BusinessPartnerAddress.results[0].to_MobilePhoneNumber.results;
				if (cellphone) {
					for (var iCellphone = 0; iCellphone < cellphone.length; iCellphone++) {
						bpData.contacts.cellphoneData[iCellphone] = {};
						bpData.contacts.cellphoneData[iCellphone].primary = cellphone[iCellphone].IsDefaultPhoneNumber;
						var cellPhoneDialingCode = this._getDialingCode(that, cellphone[iCellphone].DestinationLocationCountry);
						bpData.contacts.cellphoneData[iCellphone].cellphoneDialingCode = cellPhoneDialingCode;
						bpData.contacts.cellphoneData[iCellphone].cellphoneNo = cellphone[iCellphone].PhoneNumber;

						////**********************Cellphone Validation**************///
						bpData.contacts.cellphoneData[iCellphone].validFields = {};
						bpData.contacts.cellphoneData[iCellphone].validFields.cellphoneNo = "None";
						bpData.contacts.cellphoneData[iCellphone].validationMsg = {};
						bpData.contacts.cellphoneData[iCellphone].validationMsg.cellphoneNo = "";
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping cellphone detail from request to view, Error: " + error);
			}

			/////**************Fax********************////
			try {
				var fax = data.to_BusinessPartnerAddress.results[0].to_FaxNumber.results;
				if (fax) {
					for (var iFax = 0; iFax < fax.length; iFax++) {
						bpData.contacts.faxData[iFax] = {};
						bpData.contacts.faxData[iFax].primary = fax[iFax].IsDefaultFaxNumber;
						var faxDialingCode = this._getDialingCode(that, fax[iFax].FaxCountry);
						bpData.contacts.faxData[iFax].faxDialingCode = faxDialingCode;
						bpData.contacts.faxData[iFax].faxNo = fax[iFax].FaxNumber;

						////**********************Fax Validation**************///
						bpData.contacts.faxData[iFax].validFields = {};
						bpData.contacts.faxData[iFax].validFields.faxNo = "None";
						bpData.contacts.faxData[iFax].validationMsg = {};
						bpData.contacts.faxData[iFax].validationMsg.faxNo = "";
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping fax detail from request to view");
			}
			/////**************Email********************////
			try {
				var email = data.to_BusinessPartnerAddress.results[0].to_EmailAddress.results;
				if (email) {
					for (var iEmail = 0; iEmail < email.length; iEmail++) {
						bpData.contacts.emailData[iEmail] = {};
						bpData.contacts.emailData[iEmail].primary = email[iEmail].IsDefaultEmailAddress;
						bpData.contacts.emailData[iEmail].emailId = email[iEmail].EmailAddress;

						////**********************Email Validation**************///
						bpData.contacts.emailData[iEmail].validFields = {};
						bpData.contacts.emailData[iEmail].validFields.emailId = "None";
						bpData.contacts.emailData[iEmail].validationMsg = {};
						bpData.contacts.emailData[iEmail].validationMsg.emailId = "";
					}
				}
			} catch (error) {
				Log.info("Error occured in mapping email detail from request to view");
			}
			return bpData;
		},
		_businessPartnerRole: function (BusinessPartner) {
			// var to_BusinessPartnerRole = 
			return [{
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "BPSITE"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLCU0"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLCU1"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLVN0"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLVN1"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLCU00"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLVN01"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLCU01"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLVN00"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "UKM000"
			}];
			// return to_BusinessPartnerRole;
		},
		_ifNotThen: function (mainValue, elseValue) {
			return (mainValue ? mainValue : elseValue);
		},
		_addValidation: function (arrayField) {

		},
		_getDialingCode: function (that, code) {
			var dialingCode = that.getView().getModel("countryDialingCodeModel").getProperty("/results");
			var sCode = dialingCode.find(function (oCountryCode, index) {
				if (oCountryCode.countryCode === code) {
					return oCountryCode;
				}
			});
			return sCode.dialingCode;
		}
	};
});