sap.ui.define([], function () {
	"use strict";

	return {
		toUpperCaseChange: function (oEvent) {
			var oInput = oEvent.getSource();
			oInput.setValue(oEvent.getSource().getValue().toUpperCase());
		}
	};
});