sap.ui.define([], function () {
	"use strict";

	function _callPostService(targetURL, options) {
		return fetch(targetURL, options)
			.then(function (res) {
				if (res.ok) {
					return res.json();
				} else {
					return Promise.reject(res.status);
				}
			});
	}

	function _callGetService(targetURL, options) {
		return fetch(targetURL, options)
			.then(function (res) {
				if (res.ok) {
					return res;
				} else {
					return Promise.reject(res.status);
				}
			});
	}
	return {
		getBusinessPartner: function () {
			var targetURL = "/FS1/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner?$top=1";
			return _callGetService(targetURL);
		},
		postToBusinessProcess: function (processData) {
			var wfParameter = {};
			wfParameter.definitionId = "managebp";
			// "manage_bp";
			var targetURL = "/bpmworkflowruntime/v1/workflow-instances"; ////manage_bp
			wfParameter.context = processData;
			var xcsrfToken = "";
			/////////////***************///////////////////////////
			$.ajax({
				url: "/bpmworkflowruntime/v1/xsrf-token",
				method: "GET",
				headers: {
					"X-CSRF-Token": "Fetch"
				},
				success: function (result, xhr, data) {
					xcsrfToken = data.getResponseHeader("X-CSRF-Token");
					$.ajax({
						url: targetURL,
						// method: "PUT",
						method: "POST",
						headers: {
							"X-CSRF-Token": xcsrfToken,
							"Content-Type": "application/json"
						},
						data: JSON.stringify(wfParameter),
						success: function (result1, xhr1, data1) {},
						error: function (jqXHR, status, error) {
							sap.m.MessageToast.show("Filed to start the workflow");
						}
					});
				},
				error: function (jqXHR, status, error) {
					sap.m.MessageToast.show("Filed to fetch the XSCRF Token");
				}
			});
		},
		getXsrfToken: function () {
			var options = {
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					"X-CSRF-Token": "Fetch"
				}
			};
			return _callGetService("/bpmworkflowruntime/v1/xsrf-token", options);
		}

	};
});

//////*********************************************************************/////
//////***************Sample code for post operation for Fetch*************/////
//////*******************************************************************/////

// const user = {
//     first_name: 'John',
//     last_name: 'Lilly',
//     job_title: 'Software Engineer'
// };

// const options = {
//     method: 'POST',
//     body: JSON.stringify(user),
//     headers: {
//         'Content-Type': 'application/json'
//     }
// }

// fetch('https://reqres.in/api/users', options)
//     .then(res => res.json())
//     .then(res => console.log(res));
//////////_____________________________________________________________________//////
// onSubmit: function () {
// 			var ID = this.byId("inputId").getValue();
// 			var Name = this.byId("inputName").getValue();
// 			var Department = this.byId("inputDepartment").getValue();
// 			var Salary = this.byId("inputSalary").getValue();
// 			var emailId = this.byId("inputEmail").getValue();
// 			var post = true;
// 			if (this.byId("buttonToggle").getPressed())
// 				post = false;
// 			else
// 				post = true;
// 			var xcsrfToken = "";
// 			var wfParameter = {};

// 			var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
// 			if (!Number.isNaN(Salary) && emailId.match(rexMail)) {
// 				wfParameter.definitionId = "create";
// 				wfParameter.context = {
// 					"employee": {
// 						"ID": ID,
// 						"Name": Name,
// 						"Department": Department,
// 						"Salary": Salary
// 					},
// 					"emailId": emailId,
// 					"post": post

// 				};
// 			} else {
// 				sap.m.MessageToast.show("Enter right value");
// 				return;
// 			}

// 			$.ajax({
// 				url: "/bpmworkflowruntime/v1/xsrf-token",
// 				method: "GET",
// 				headers: {
// 					"X-CSRF-Token": "Fetch"
// 				},
// 				success: function (result, xhr, data) {
// 					xcsrfToken = data.getResponseHeader("X-CSRF-Token");

// 					$.ajax({
// 						url: "/bpmworkflowruntime/v1/workflow-instances",
// 						method: "POST",
// 						headers: {
// 							"X-CSRF-Token": xcsrfToken,
// 							"Content-Type": "application/json"
// 						},
// 						data: JSON.stringify(wfParameter),
// 						success: function (result, xhr, data) {
// 							sap.m.MessageToast.show("Work Flow started successfuly");
// 						},
// 						error: function (jqXHR, status, error) {
// 							sap.m.MessageToast.show("Filed to start the workflow");
// 						}
// 					});
// 				},
// 				error: function (jqXHR, status, error) {
// 					sap.m.MessageToast.show("Filed to fetch the XSCRF Token");
// 				}
// 			});
// 			this.byId("inputId").setValue("");
// 			this.byId("inputName").setValue("");
// 			this.byId("inputDepartment").setValue("");
// 			this.byId("inputSalary").setValue("");
// 			this.byId("inputEmail").setValue("");
// 			// debugger;
// 		}