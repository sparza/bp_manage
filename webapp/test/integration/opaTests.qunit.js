/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"bp/spar/co/za/BusinessPartner/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});
