sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	// "sap/ui/core/Core",
	// "sap/ui/core/library",
	// "sap/ui/core/message/Message",
	// "sap/m/MessagePopover",
	// "sap/m/MessagePopoverItem",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment",
	"bp/spar/co/za/BusinessPartner/services/LookupService",
	"bp/spar/co/za/BusinessPartner/services/util",
	"bp/spar/co/za/BusinessPartner/services/BusinessPartnerService",
	"bp/spar/co/za/BusinessPartner/services/validation",
	"bp/spar/co/za/BusinessPartner/services/buildingBPService"
], function (BaseController, JSONModel, formatter, MessageBox, Fragment, LookupService, UtilFunction, PostService, ValidationService,
	BuildingBPService) {
	// ], function (BaseController, JSONModel, formatter, Core, coreLibrary, Message, MessagePopover,
	// 	MessagePopoverItem, Fragment, LookupService, PostService, ValidationService, BuildingBPService) {
	"use strict";

	return BaseController.extend("bp.spar.co.za.BusinessPartner.controller.Object", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function () {
			// debugger;
			// this.getOwnerComponent().getRouter("A_BusinessPartner/{objectId}").attachPatternMatched(this._onObjectMatched, this);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("object").attachPatternMatched(this._onObjectMatched, this);
			// oRouter.getRoute("ObjectPage").attachPatternMatched(this._onObjectMatched, this);

			////**********************************************************************************
			// var bpData = {};
			// // var oStorage = {};
			// // bpData.contacts = {};
			// bpData.generalData = {};
			// bpData.address = {};
			// // bpData.bpData = [];
			// bpData.company = [];

			// // var that = this;
			// var modelName = "pModel";
			// // var literalModel = this.getView().getModel("literalModel");
			// this.getView().setModel(new JSONModel(bpData), modelName);
			// debugger;
			// this._setDefaultValues();

			/////////////____________Define Different Models__________________//////
			this.getView().setModel(new JSONModel({}), "compModel");

			////******************  ***********************////
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(this._handleRouteMatched, this);

		},

		_handleRouteMatched: function (oEvent) {
			//Check whether is the detail page is matched.
			if (oEvent.getParameter("name") !== "object") {
				return;
			} else {
				// debugger;
				var that = this;
				var viewFragmentIds = ["idSimpleFormGeneralChange", "idSimpleFormAddressChange"];
				var requiredFieldIds = this._getRequiredFieldsFromFragments(viewFragmentIds);
				requiredFieldIds.forEach(function (oValue) {
					if (that.getView().byId(oValue).getValueState() === "Error") {
						that.getView().byId(oValue).setValueState("None");
						that.getView().byId(oValue).setValueStateText("");
					}
				});
			}
		},
		_getRequiredFieldsFromFragments: function (viewFragmentIds) {
			// var viewFragmentIds = ["idSimpleFormGeneralChange", "idSimpleFormAddressChange"];
			var requiredFields = [];
			// viewFragmentIds.forEach(callbackFn, [thisArg])
			for (var iView = 0; iView < viewFragmentIds.length; iView++) {
				var fContents = this.getView().byId(viewFragmentIds[iView]).getContent();
				fContents.forEach(function (oValue) {
					try {
						if (oValue.isRequired()) {
							requiredFields.push(oValue.getLabelForRendering());
						}
					} catch (error) {}
				});
			}
			return requiredFields;
		},
		_onObjectMatched: function (oEvent) {
			var sObjectId = oEvent.getParameter("arguments").objectId;
			if (sObjectId === "-1") {
				this.getView().byId("idCode").setEditable(true);
				// debugger;
				// jQuery.sap.storage(jQuery.sap.storage.Type.local).remove("BusinessPartner.LocalData");
				this._setDefaultValues();
			} else {
				if (this.getView().getModel("appModel").getProperty("/AppData").actionCreateALike) {
					this.getView().getModel("processModel").getProperty("/generalData").code = "";
					this.getView().byId("idCode").setEditable(true);
				} else {
					this.getView().byId("idCode").setEditable(false);
				}
				this.getView().setModel(new JSONModel(this.getView().getModel("processModel").getProperty("/")), "pModel");
				this._callLookupServices(this);
			}
			return sObjectId;
		},
		_setDefaultValues: function () {
			// var validationData = {};
			var literalModel = this.getView().getModel("literalModel");
			var oTelephoneData = {
				"primary": true,
				"telephoneName": "",
				"idTelephoneTabContactType": "",
				"telephoneDialingCode": literalModel.getProperty("/").countrydialingCode, //"+27", //this.getView().getModel("literalModel").getProperty("/").countrydialingCode, //"+27",
				"telephoneNo": "",
				"telephoneExtension": "",
				"validFields": {
					"telephoneNo": "None"
				},
				"validationMsg": {
					"telephoneNo": ""
				}
			};
			// this.getView().setModel(new JSONModel(validationData), "validationModel");
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var bpData = {};
			var data = oStorage.get("BusinessPartner.LocalData");
			if (data) {
				bpData = data;
			} else {
				bpData.generalData = {};
				bpData.address = {};
				bpData.company = [];
			}
			var modelName = "pModel";
			this.getView().setModel(new JSONModel(bpData), modelName);

			var oModel = this.getView().getModel("pModel");

			if (!oModel.getProperty("/contacts")) {
				oModel.setProperty("/contacts", {});
				oModel.setProperty("/contacts/telephoneData", []);
				oModel.getProperty("/contacts/telephoneData").push(oTelephoneData);
			}
			this.getView().setModel(new JSONModel(bpData), modelName);
			if (!bpData.address.hasOwnProperty("countrySAddress")) {
				// this._bindingWithFragment("idSimpleFormAddressChange", "/address", {});
				this.getView().getModel(modelName).setProperty("/address/countrySAddress", literalModel.getProperty("/").countryCode); //"ZA");
			}
			if (!bpData.address.hasOwnProperty("countryPAddress")) {
				this.getView().getModel(modelName).setProperty("/address/countryPAddress", literalModel.getProperty("/").countryCode); //"ZA");
			}
			if (!bpData.generalData.hasOwnProperty("countryGroupDetail")) {
				this.getView().getModel(modelName).setProperty("/generalData/countryGroupDetail", literalModel.getProperty("/").countryCode); //"ZA");
			}
			this._callLookupServices(this);
			// this.callGetProvinceCodeLookupService(this, "/address/countrySAddress", "provinceSAModel");
		},
		draftSave: function () {
			// debugger;
			var modelName = "pModel";
			var that = this;
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			oStorage.clear();
			var data = that.getView().getModel(modelName).getProperty("/");
			oStorage.put("BusinessPartner.LocalData", data);
			var oDraftIndicator = that.getView().byId("idDraftIndicator");
			oDraftIndicator.showDraftSaving();
			oDraftIndicator.showDraftSaved();
			oDraftIndicator.clearDraftState();
		},
		_callLookupServices: function (that) {
			var modelName = "pModel";
			LookupService.getPaymentMethodsList(this.getView().getModel(modelName).getProperty("/address/countrySAddress")).then(function (
				res) {
				var PaymentMethodModel = new JSONModel(res.d);
				PaymentMethodModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(PaymentMethodModel, "PaymentMethodModel");
			});
			that.callGetProvinceCodeLookupService(that, "/address/countrySAddress", "provinceSAModel");
			that.callGetProvinceCodeLookupService(that, "/address/countryPAddress", "provincePAModel");
		},
		onClickAddCompany: function (oEvent) {
			var oCompany = {
				"companyCode": "",
				"reconciliationAccount": "",
				"paymentTerms": this.getView().getModel("literalModel").getProperty("/").paymentTerms, //"X01S",
				"paymentMethods": this.getView().getModel("literalModel").getProperty("/").paymentMethods, //"D",
				"userAtCustomer": "",
				"accountingClerkEmails": "",
				"validFields": {
					"companyCode": "None",
					"userAtCustomer": "None",
					"accountingClerkEmails": "None"
				},
				"validationMsg": {
					"companyCode": "",
					"userAtCustomer": "",
					"accountingClerkEmails": ""
				}
			};
			this.addNewRow(oEvent, oCompany);
		},
		_bindingWithFragment: function (idFragment, sPath, oType) {
			this.getView().getModel("pModel").setProperty(sPath, oType);
			this.getView().byId(idFragment).bindElement({
				path: sPath,
				model: "pModel"
			});
		},
		onCountrySAChange: function (oEvent) {
			this.callGetProvinceCodeLookupService(this, "/address/countrySAddress", "provinceSAModel");
			this.callGetDialingCodeLookupService(this, "/address/countrySAddress", "dialingCodeModel");
		},
		onCountryPAChange: function (oEvent) {
			this.callGetProvinceCodeLookupService(this, "/address/countryPAddress", "provincePAModel");
		},
		callGetProvinceCodeLookupService: function (that, propertyURL, modelName) {
			LookupService.getProvinceCode(this.getView().getModel("pModel").getProperty(propertyURL)).then(function (response) {
				var provinceModel = new JSONModel(response.d);
				provinceModel.setSizeLimit(response.d.results.length);
				that.getOwnerComponent().setModel(provinceModel, modelName);
			});
		},
		callGetDialingCodeLookupService: function (that, propertyURL, modelName) {
			LookupService.getDialingCode(this.getView().getModel("pModel").getProperty(propertyURL)).then(function (response) {
				var dialingCodeModel = new JSONModel(response.d);
				dialingCodeModel.setSizeLimit(response.d.results.length);
				that.getOwnerComponent().setModel(dialingCodeModel, modelName);
			});
		},

		onBankTableAddPress: function (oEvent) {
			// var dialingCode = "";
			// try {
			// 	dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].countryCode;
			// } catch (ex) {
			// 	dialingCode = this.getView().getModel("literalModel").getProperty("/").countryCode; //"+27";
			// }
			var oBank = {
				"countryBankDetail": this.getView().getModel("literalModel").getProperty("/").countryCode,
				"bankNameBank": this.getView().getModel("literalModel").getProperty("/").bankNameBank,
				"bankCodeBank": "",
				"accountNumberBank": "",
				"accountNameBank": "",
				"bankUsedForBank": "",
				"validFields": {
					"bankCodeBank": "None",
					"accountNumberBank": "None",
					"accountNameBank": "None"
				},
				"validationMsg": {
					"bankCodeBank": "",
					"accountNumberBank": "",
					"accountNameBank": ""
				}
			};
			this.addNewRow(oEvent, oBank);
			var controlId = oEvent.getSource().getParent().getParent();
			this.getView().byId(controlId.getId()).setModel(this.getOwnerComponent().getModel("bankNameModel"), "bankNameModel");
		},
		onEmailAddPress: function (oEvent) {
			var oEmail = {
				"primary": true,
				"emailContactType": "",
				"emailId": "",
				"validFields": {
					"emailId": "None"
				},
				"validationMsg": {
					"emailId": ""
				}
			};
			this.addNewRow(oEvent, oEmail);
		},

		onCellphoneAddPress: function (oEvent) {
			var dialingCode = "";
			try {
				dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].dialingCode;
			} catch (ex) {
				dialingCode = this.getView().getModel("literalModel").getProperty("/").countrydialingCode; //"+27";
			}
			var oCellphone = {
				"primary": true,
				"cellphoneContactType": "",
				"cellphoneDialingCode": dialingCode,
				"cellphoneNo": "",
				"cellphoneName": "",
				"validFields": {
					"cellphoneNo": "None"
				},
				"validationMsg": {
					"cellphoneNo": ""
				}
			};
			this.addNewRow(oEvent, oCellphone);
		},

		onFaxAddPress: function (oEvent) {
			var dialingCode = "";
			try {
				dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].dialingCode;
			} catch (ex) {
				dialingCode = this.getView().getModel("literalModel").getProperty("/").countrydialingCode; //"+27";
			}

			var oFaxData = {
				"primary": true,
				"faxContactType": "",
				"faxDialingCode": dialingCode,
				"faxNo": "",
				"faxExtension": "",
				"faxName": "",
				"validFields": {
					"faxNo": "None"
				},
				"validationMsg": {
					"faxNo": ""
				}
			};
			this.addNewRow(oEvent, oFaxData);
		},

		onTelephoneTableAddPress: function (oEvent) {
			var dialingCode = "";
			try {
				dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].dialingCode;
			} catch (ex) {

				dialingCode = this.getView().getModel("literalModel").getProperty("/").countrydialingCode; //"+27";
			}
			var oTelephoneData = {
				"primary": false,
				"telephoneName": "",
				"idTelephoneTabContactType": "",
				"telephoneDialingCode": dialingCode,
				"telephoneNo": "",
				"telephoneExtension": "",
				"validFields": {
					"telephoneNo": "None"
				},
				"validationMsg": {
					"telephoneNo": ""
				}
			};
			this.addNewRow(oEvent, oTelephoneData);
		},

		onRowDeletePress: function (oEvent) {
			var oPath = oEvent.getSource().getParent().getParent().getBinding().sPath;
			var controlId = oEvent.getSource().getParent().getParent().getId();
			var modelName = "pModel";
			var oModel = this.byId(controlId).getModel(modelName);
			// if (this.byId(controlId).getSelectedIndex() !== -1) {
			var iIndex = oEvent.getSource().getParent().getIndex(); //this.byId(controlId).getSelectedIndices()[0];
			///*******check if the selected one is primary then make the first as primary
			if (oModel.getProperty(oPath)[iIndex].primary === true && oModel.getProperty(oPath).length !==
				1) {
				oModel.getProperty(oPath).splice(iIndex, 1);
				oModel.getProperty(oPath)[0].primary = true;
			} else {
				oModel.getProperty(oPath).splice(iIndex, 1);
			}
			this.byId(controlId).clearSelection();
			oModel.refresh(true);
		},

		addNewRow: function (oEvent, oData) {
			var modelName = "pModel";
			// var oTelephoneData = oData;
			var oModel = this.getView().getModel(modelName);
			var that = this;
			var oPath = oEvent.getSource().getParent().getParent().getBinding().sPath;
			var controlId = oEvent.getSource().getParent().getParent();
			if (!oModel.getProperty(oPath)) {
				oModel.setProperty(oPath, []);
				oModel.getProperty(oPath).push(oData);
				this.getView().byId(controlId.getId()).setModel(this.getView().getModel(modelName), modelName);
				this.getView().byId(controlId.getId()).getModel(modelName).refresh(true);
			} else {
				var aLength = oModel.getProperty(oPath).length;
				if (aLength === 0) {
					oModel.getProperty(oPath).push(oData);
					this.getView().byId(controlId.getId()).getModel(modelName).refresh(true);
				} else {
					var tableInfo = oEvent.getSource().getParent().getParent();
					var valid = ValidationService.validateOnAddInArrayFields(that, tableInfo);

					if (valid) {
						oData.primary = false;
						// oModel.getProperty(oPath).push(oData);
						oModel.getProperty(oPath).unshift(oData);
						this.getView().byId(controlId.getId()).getModel(modelName).refresh(true);
					}
					// }
				}
			}
		},

		toUpperCaseChange: function (oEvent) {
			UtilFunction.toUpperCaseChange(oEvent);
		},

		// Function for the SUBMIT button press
		onSubmitPress: function (oEvent) {
			///**************Checking validation
			var flagToSubmit = true;
			var pModel = "pModel";
			this.getView().getModel("validationModel").setProperty("/", []);
			var oView = this.getView();
			var objectPageId = "idObjectPageLayout";
			var requiredFieldIds = this._getRequiredFieldsFromFragments(["idSimpleFormGeneralChange", "idSimpleFormAddressChange"]); //viewFragmentIds);
			flagToSubmit = ValidationService.checkInputConstraints(this, requiredFieldIds);

			////*****need not to check 2nd validation if mandetory field validation returning true
			if (flagToSubmit) {
				var arrayIds = [
					oView.byId("idTelephoneTableContactDetail"),
					oView.byId("idFaxTableContactDetail"),
					oView.byId("idCellphoneTableContactDetail"),
					oView.byId("idEmailTableContactDetail"),
					oView.byId("idBankDetail"),
					oView.byId("idCompanyDetail")
				];
				flagToSubmit = ValidationService.checkInputConstraints(this, arrayIds, true);
			}

			if (flagToSubmit) { //
				var processData = {};
				// this.getView().getModel(pModel).setProperty("/user", this.getView().getModel("userModel").getProperty("/"));
				// this.getView().getModel("appModel").getProperty("/AppData")["user"] = this.getView().getModel("userModel").getProperty("/");
				processData.user = this.getView().getModel("userModel").getProperty("/");
				var data = this.getView().getModel(pModel).getProperty("/");
				this.SetPageBusy(oEvent);
				processData.actionCreate = this.getView().getModel("appModel").getProperty("/AppData").actionCreate ? this.getView().getModel(
					"appModel").getProperty("/AppData").actionCreate : this.getView().getModel("appModel").getProperty("/AppData").actionCreateALike;

				processData.actionUpdate = this.getView().getModel("appModel").getProperty("/AppData").actionUpdate;
				///**********this is temporary*********///
				// processData.actionCreate = true;
				// processData.actionUpdate = false;
				////*****************************************/////				
				processData.request = {};
				processData.request.data = {};
				processData.request.inBp = {};
				if (processData.actionCreate) {
					processData.request.data = BuildingBPService.buildServiceObjectMapFromView_Create(data);
				} else {
					var bpResponse = (this.getView().getModel(pModel).getProperty("/bPResponse") ? this.getView().getModel(pModel).getProperty(
						"/bPResponse") : {});
					processData.request.data = BuildingBPService.buildServiceObjectMapFromView_Update(data, bpResponse);
				}
				processData.request.inBp = this.getView().getModel(pModel).getProperty("/");

				////////////******************Building data for the Approval Screen*******************//
				processData.request.inBp = BuildingBPService.buildingBPToApprovalView(this, processData.request.inBp);

				PostService.postToBusinessProcess(processData);
				var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
				oStorage.clear("BusinessPartner.LocalData");

				this.getView().getModel(pModel).setProperty("/", {});
				this.getView().getModel(pModel).setProperty("/countryGroupDetail", this.getView().getModel("literalModel").countryCode); //"ZA");
				// this.getView().getModel("pModel").setProperty("/openingReasonGeneralDetail", "3");
				// this.getView().getModel("pModel").setProperty("/corporateStoreIndicatorGeneralDetail", "0001");
				this.getView().getModel(pModel).setProperty("/address", {});

				this.getView().getModel(pModel).getProperty("/company", []);
				this.getView().getModel(pModel).refresh(true);
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("worklist", true);

			} else {
				this.getView().byId(objectPageId).setSelectedSection(this.getView().byId(this.getView().getModel("validationModel").getProperty(
					"/")[0]).getId());
			}

		},
		OnCancelPress: function (oEvent) {
			// var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			// oStorage.remove("BusinessPartner.LocalData");
			// this.getView().getModel("pModel").setProperty("/", {});
			// //**** go back to the search page
			// var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			// oRouter.navTo("worklist", true);

			// var bpData = {};
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var data = oStorage.get("BusinessPartner.LocalData");
			// var that = this;
			if (data) {
				this.onMessageBoxPress(this);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("worklist", true);
			}

		},
		onMessageBoxPress: function (that) {
			MessageBox.confirm("Do you want to save draft data.", {
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				emphasizedAction: MessageBox.Action.YES,
				onClose: function (sAction) {
					var oStorage = {};
					if (sAction === "NO") {
						// if (sAction === "YES") {
						// 	oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
						// 	var bpData = oStorage.get("BusinessPartner.LocalData");
						// 	that.getView().setModel(new JSONModel(bpData), "pModel");
						// } else {
						oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
						oStorage.remove("BusinessPartner.LocalData");
						// that.getView().getModel("pModel").setProperty("/", {});
					}
					var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
					oRouter.navTo("worklist", true);
				}
			});
		},
		SetPageBusy: function (oEvent) {
			var oPanel = this.getView().byId("idMainPage");
			oPanel.setBusy(true);
			// simulate delayed end of operation
			window.setTimeout(function () {
				oPanel.setBusy(false);
			}, 3000);
			window.setTimeout(function () {
				sap.m.MessageToast.show("Work Flow started successfuly");
			}, 3500);
		},

		// onAfterItemAdded: function (oEvent) {},
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.ObjectPage
		 */
		onBeforeRendering: function () {

			var modelName = "pModel";
			var bpData = this.getView().getModel(modelName).getProperty("/");
			// var literalModel = this.getView().getModel("literalModel");
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			// oStorage.put("myLocalData", data);
			if (oStorage.get("BusinessPartner.LocalData")) {
				bpData = oStorage.get("BusinessPartner.LocalData");
				if (bpData.generalData.liquorLicenceNumberStartDate) {
					bpData.generalData.liquorLicenceNumberStartDate = new Date(bpData.generalData.liquorLicenceNumberStartDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.liquorLicenceNumberEndDate) {
					bpData.generalData.liquorLicenceNumberEndDate = new Date(bpData.generalData.liquorLicenceNumberEndDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.grocerWineLicenceNumberStartDate) {
					bpData.generalData.grocerWineLicenceNumberStartDate = new Date(bpData.generalData.grocerWineLicenceNumberStartDate); //"2020-04-20T09:22:03.092Z"
				}
				// debugger;
				if (bpData.generalData.grocerWineLicenceNumberEndDate) {
					bpData.generalData.grocerWineLicenceNumberEndDate = new Date(bpData.generalData.grocerWineLicenceNumberEndDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.distributionLicencNumberStartDate) {
					bpData.generalData.distributionLicencNumberStartDate = new Date(bpData.generalData.distributionLicencNumberStartDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.distributionLicencNumberEndDate) {
					bpData.generalData.distributionLicencNumberEndDate = new Date(bpData.generalData.distributionLicencNumberEndDate); //"2020-04-20T09:22:03.092Z"
				}
			}
		},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.ObjectPage
		 */
		// onAfterRendering: function () {
		// 	debugger;
		// },

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.ObjectPage
		 */
		// onExit: function () {
		// 	var data = this.getView().getModel("pModel").getProperty("/");
		// 	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		// 	oStorage.put("myLocalData", data);
		// }

	});

});