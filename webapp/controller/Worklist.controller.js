sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"bp/spar/co/za/BusinessPartner/services/BusinessPartnerService",
	"bp/spar/co/za/BusinessPartner/services/LookupService",
	"bp/spar/co/za/BusinessPartner/services/util",
	"bp/spar/co/za/BusinessPartner/services/buildingBPService"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator, MessageBox, BPService, LookupService,
	UtilFunction, BuildingBPService) {
	"use strict";

	return BaseController.extend("bp.spar.co.za.BusinessPartner.controller.Worklist", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function () {
			this.getView().setModel(new JSONModel({
				"filterData": {}
			}), "filterModel");
			//*****temp***********
			var that = this;
			this._callLookupServices(that);
			// var appModel = this.getView().getModel("appModel");
			// appModel.setProperty("/AppData", {});
		},
		_callLookupServices: function (that) {
			LookupService.getUserInfo().then(
				function (res) {
					// debugger;
					that.getView().getModel("userModel").setProperty("/", res);
				});
			LookupService.getCountryList().then(function (res) {
					var countryData = res.d;
					countryData.results.push({
						countryCode: "",
						countryDescription: ""
					});

					var countryModel = that.getView().getModel("countryModel");
					countryModel.setSizeLimit(countryData.results.length);
					countryModel.setProperty("/", countryData);
				},
				function (error) { /* handle an error */
					// debugger;
				}
			);
			LookupService.getBankNameList().then(function (res) {
					var bankNameData = res.d;
					that.getView().getModel("bankNameModel").setProperty("/", bankNameData);
				},
				function (error) { /* handle an error */
					// debugger;
				}
			);
			LookupService.getPaymentTermsList().then(function (res) {
					var results = res.d.results;
					var result = results.filter(function (el) {
						if (el.description !== "") {
							return el;
						}
					});
					res.d.results = result;
					var PaymentTermsData = res.d;
					that.getView().getModel("PaymentTermsModel").setProperty("/", PaymentTermsData);
				},
				function (error) { /* handle an error */
					// debugger;
				}
			);
			LookupService.getCountryCode().then(function (response) {
					var dialingCodeData = response.d;
					var dialingCodeModel = that.getView().getModel("countryDialingCodeModel");
					dialingCodeModel.setSizeLimit(dialingCodeData.results.length);
					dialingCodeModel.setProperty("/", dialingCodeData);
					// that.getView().setModel(dialingCodeModel, "countryDialingCodeModel");
					// that.getOwnerComponent().setModel(dialingCodeModel, "dialingCodeModel");
				},
				function (error) { /* handle an error */
					// debugger;
				}
			);
			LookupService.getDeliveryServiceTypeList().then(function (response) {
					var deliveryServiceData = response.d;
					deliveryServiceData.results.push({
						code: "",
						description: ""
					});
					var deliveryServiceModel = that.getView().getModel("deliveryServiceModel");
					deliveryServiceModel.setSizeLimit(deliveryServiceData.results.length);
					deliveryServiceModel.setProperty("/", deliveryServiceData);
					// that.getView().setModel(dialingCodeModel, "countryDialingCodeModel");
					// that.getOwnerComponent().setModel(dialingCodeModel, "dialingCodeModel");
				},
				function (error) { /* handle an error */
					// debugger;
				}
			);

		},
		onPressCreateALike: function (oEvent) {
			// // var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getSource().getItemNavigation()
			// // 	.iFocusedIndex - 1].BusinessPartner;
			// // var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getParameters().id[oEvent.getParameters().id.length-1]];
			// // this._getBPDetail(oEvent, businessPartner);
			// if (oEvent.getParameters().id.search("idView") !== -1) {
			// 	////its a BP Edit flow
			// } else {
			/////its a create a like flow
			var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getParameters().id.substring(
				oEvent.getParameters().id.lastIndexOf("-") + 1, oEvent.getParameters().id.length)].BusinessPartner;
			this.getView().getModel("appModel").setProperty("/AppData", {
				"actionCreate": false,
				"actionCreateALike": true,
				"actionUpdate": false
			});
			this._getBPDetail(oEvent, businessPartner);
			// }

		},
		onPressUpdate: function (oEvent) {
			// var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getSource().getItemNavigation()
			// 	.iFocusedIndex - 1].BusinessPartner;
			// var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getParameters().id[oEvent.getParameters().id.length-1]];
			// this._getBPDetail(oEvent, businessPartner);
			// if (oEvent.getParameters().id.search("idView") !== -1) {
			// 	////its a BP Edit flow
			// } else {
			/////its a create a like flow
			var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getParameters().id.substring(
				oEvent.getParameters().id.lastIndexOf("-") + 1, oEvent.getParameters().id.length)].BusinessPartner;
			this.getView().getModel("appModel").setProperty("/AppData", {
				"actionCreate": false,
				"actionCreateALike": false,
				"actionUpdate": true
			});
			this._getBPDetail(oEvent, businessPartner);
			// }

		},
		// onPressTableRow: function (oEvent) {

		// 	// var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getSource().getItemNavigation()
		// 	// 	.iFocusedIndex - 1].BusinessPartner;
		// 	// var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getParameters().id[oEvent.getParameters().id.length-1]];
		// 	// this._getBPDetail(oEvent, businessPartner);
		// 	if (oEvent.getParameters().id.search("idView") !== -1) {
		// 		////its a BP Edit flow
		// 	} else {
		// 		/////its a create a like flow
		// 		var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/").results[oEvent.getParameters().id[oEvent.getParameters()
		// 			.id.length - 1]].BusinessPartner;
		// 		this.getView().getModel("appModel").setProperty("/AppData", {
		// 			"actionCreate": true
		// 		});
		// 		this._getBPDetail(oEvent, businessPartner);
		// 	}

		// },
		onPressAdd: function (oEvent) {
			var bpData = {};
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var data = oStorage.get("BusinessPartner.LocalData");
			// var that = this;
			if (data) {
				this.onMessageBoxPress(this);
			} else {
				bpData.generalData = {};
				bpData.address = {};
				bpData.company = [];
				this.getView().getModel("appModel").setProperty("/AppData", {
					"actionCreateALike": false,
					"actionCreate": true,
					"actionUpdate": false
				});
				this.getOwnerComponent().getRouter().navTo("object", {
					objectId: "-1"
				}, true);
				this.getView().setModel(new JSONModel(bpData), "pModel");
			}
		},
		_getBPDetail: function (oEvent, bpCode) {
			// debugger;
			var targetURL = "",
				that = this;
			targetURL = "A_BusinessPartner(BusinessPartner='" + bpCode +
				"')?$format=json&$expand=to_BuPaIdentification,to_BusinessPartnerAddress,to_BusinessPartnerAddress/to_EmailAddress,to_BusinessPartnerAddress/to_FaxNumber,to_BusinessPartnerAddress/to_MobilePhoneNumber,to_BusinessPartnerAddress/to_PhoneNumber,to_BusinessPartnerAddress/to_URLAddress,to_BusinessPartnerBank,to_BusinessPartnerContact,to_BusinessPartnerContact/to_ContactAddress,to_BusinessPartnerContact/to_ContactRelationship,to_BusinessPartnerRole,to_BusinessPartnerTax,to_Customer,to_Customer/to_CustomerCompany,to_Customer/to_CustomerCompany/to_CustomerDunning,to_Customer/to_CustomerCompany/to_WithHoldingTax,to_Customer/to_CustomerSalesArea,to_Customer/to_CustomerSalesArea/to_PartnerFunction,to_Customer/to_CustomerSalesArea/to_SalesAreaTax,to_Supplier,to_Supplier/to_SupplierCompany,to_Supplier/to_SupplierCompany/to_SupplierDunning,to_Supplier/to_SupplierCompany/to_SupplierWithHoldingTax,to_Supplier/to_SupplierPurchasingOrg,to_Supplier/to_SupplierPurchasingOrg/to_PartnerFunction";
			LookupService.getBusinessPartnerView(targetURL).then(function (res) {
					//******* converting BP to view
					// debugger;
					// var modelName = "pModel";
					// var processModel = that.getView().getModel("pModel");
					// processModel.setProperty("/", BuildingBPService.buildingBPViewFromService(res.d));
					// var BPCode = that.getView().getModel("pModel").getProperty("/generalData/code");

					var processModel = that.getView().getModel("processModel");
					processModel.setProperty("/", BuildingBPService.buildingBPViewFromService(res.d, that));
					var BPCode = that.getView().getModel("processModel").getProperty("/generalData/code");
					processModel.getProperty("/").bPResponse = res.d;

					// that.getView().getModel("appModel").setProperty("/AppData", {
					// 	"actionCreate": false
					// });
					that._navToObject(BPCode);
				},
				function (error) { /* handle an error */
					// debugger;
				}
			);
		},
		_navToObject: function (oItem) {
			// debugger;
			// this.getRouter().navTo("createBP", {
			// 	objectId: oItem.getBindingContext().getProperty("BusinessPartner")
			// });
			this.getOwnerComponent().getRouter().navTo("object", {
				objectId: oItem
			}, true);
		},
		onPressSearch: function (oEvent) {
			// debugger;
			var searchParameters = this.getView().getModel("filterModel").getProperty("/filterData");
			var recordCount = "100";
			this._getBusinessPartnerMain(searchParameters, recordCount);
		},
		_getBusinessPartnerMain: function (searchFilter, recordCount) {
			var targetURL = "A_BusinessPartnerTaxNumber?$filter=(",
				that = this,
				bPCode = searchFilter.bPCode,
				bPName = searchFilter.bPName,
				vatRegNo = searchFilter.vATRegistrationNo,
				compRegNo = searchFilter.companyRegistrationNo;
			var urlArray = [];
			var len = urlArray.length;
			var vatPromise, compPromise, codeNamePromise;
			if (vatRegNo) {
				urlArray[len] = [];
				urlArray[len][0] = "BPTaxNumber";
				urlArray[len][1] = vatRegNo;
				targetURL = targetURL + this._createURL(urlArray, " and ") + ")&$format=json";
				vatPromise = new Promise(function (resolve, reject) {
					resolve(LookupService.getBusinessPartnerView(targetURL).then(function (res) {
						return res.d.results;
					}));
				});
			}
			targetURL = "A_BusinessPartnerTaxNumber?$filter=(";
			if (compRegNo) {
				urlArray[len] = [];
				urlArray[len][0] = "BPTaxNumber";
				urlArray[len][1] = compRegNo;
				targetURL = targetURL + this._createURL(urlArray, " and ") + ")&$format=json";
				compPromise = new Promise(function (resolve, reject) {
					resolve(LookupService.getBusinessPartnerView(targetURL).then(function (res) {
						return res.d.results;
					}));
				});
			}
			if (bPCode || bPName) {
				urlArray = [];
				len = urlArray.length;
				if (bPCode) {
					urlArray[len] = [];
					urlArray[len][0] = "BusinessPartner";
					urlArray[len][1] = bPCode;
					len = len + 1;
				}
				if (bPName) {
					urlArray[len] = [];
					urlArray[len][0] = "OrganizationBPName1";
					urlArray[len][1] = bPName;
				}
				targetURL = "A_BusinessPartner?$format=json&$expand=to_BusinessPartnerTax,to_Customer&$top=1000&$filter=(" + that._createURL(
						urlArray, " and ") +
					")";
				codeNamePromise = new Promise(function (resolve, reject) {
					resolve(LookupService.getBusinessPartnerView(targetURL).then(function (res) {
						return res.d.results;
					}));
				});
			}
			///***********this will execute if there is value in (code || name)==true and value in (vat || company)==true/false
			if (codeNamePromise) {
				Promise.all([
					vatPromise,
					compPromise,
					codeNamePromise
				]).then(function (res) {
					var compVatBP = [],
						finalResult = [];
					if (res[0] || res[1]) {
						compVatBP = that._callVatCompServices(res);
					} else {
						///***********this will execute if ther is value in (code || name)==true and no value in (vat || company)==false	
						finalResult = res[2];
					}
					///********compare compvat result output with the code name result
					///***********this will execute if ther is value in (code || name)==true and value in (vat || company)==true	
					if (codeNamePromise && (res[0] || res[1])) {
						for (var k = 0; k < compVatBP.length; k++) {
							for (var l = 0; l < res[2].length; l++) {
								if (compVatBP[k] === res[2][l].BusinessPartner) {
									finalResult[finalResult.length] = res[2][l];
									res[2].splice(l, 1);
									continue;
								}
							}
						}
					}
					finalResult.results = finalResult;
					var BPSearchResultModel = new JSONModel(finalResult);
					BPSearchResultModel.setSizeLimit(recordCount);
					//searchResult.results.length);
					that.getOwnerComponent().setModel(BPSearchResultModel, "BPSearchResultModel");
				});
			} ///***********this will execute if ther is value in (code || name)==false and value in (vat || company)==true
			else {
				Promise.all([
					vatPromise,
					compPromise
				]).then(function (res) {
					var compVatBP = [],
						finalResult = [];
					if (res[0] || res[1]) {
						compVatBP = that._callVatCompServices(res);
						urlArray = [];
						for (var i = 0; i < compVatBP.length; i++) {
							urlArray[i] = [];
							urlArray[i][0] = "BusinessPartner";
							urlArray[i][1] = compVatBP[i];
						}
						targetURL = "A_BusinessPartner?$format=json&$expand=to_BusinessPartnerTax&$top=" + recordCount + "&$filter=(" + that._createURL(
							urlArray, " or ") + ")";
						LookupService.getBusinessPartnerView(targetURL).then(function (resp) {
							finalResult = resp.d;
							var BPSearchResultModel = new JSONModel(finalResult);
							BPSearchResultModel.setSizeLimit(recordCount);
							//searchResult.results.length);
							that.getOwnerComponent().setModel(BPSearchResultModel, "BPSearchResultModel");
							return res.d.results;
						});
					}
				});
			}
		},
		toUpperCaseChange: function (oEvent) {
			UtilFunction.toUpperCaseChange(oEvent);
		},
		_callVatCompServices: function (res) {
			var compVatBP = [];
			if (res[0] && res[1]) {
				for (var i = 0; i < res[0].length; i++) {
					for (var j = 0; j < res[1].length; j++) {
						if (res[0][i].BusinessPartner === res[1][j].BusinessPartner) {
							compVatBP[compVatBP.length] = res[1][j].BusinessPartner;
							res[1].splice(j, 1);
							continue;
						}
					}
				}
			} else if (res[0]) {
				for (var ii = 0; ii < res[0].length; ii++) {
					compVatBP[compVatBP.length] = res[0][ii].BusinessPartner;
				}
			} else if (res[1]) {
				for (var jj = 0; jj < res[1].length; jj++) {
					compVatBP[compVatBP.length] = res[1][jj].BusinessPartner;
				}
			}
			return compVatBP;
		},
		_createURL: function (urlArray, operation) {
			var URL = "";
			if (urlArray.length > 1) {
				for (var i = 0; i < urlArray.length; i++) {
					URL = URL + "substringof('" + urlArray[i][1] + "'," + urlArray[i][0] + ")";
					if (i < urlArray.length - 1) {
						URL = URL + operation;
					}
				}
			} else {
				URL = "substringof('" + urlArray[0][1] + "'," + urlArray[0][0] + ")";
			}
			return URL;
		},
		_createSearchResultRecord: function (respObject) {
			var searchResultObj = {};
			searchResultObj.code = respObject.BusinessPartner;
			searchResultObj.name = respObject.OrganizationBPName1;
			return searchResultObj;
		},
		onMessageBoxPress: function (that) {
			MessageBox.confirm("Do you want to load previously saved draft data.", {
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				emphasizedAction: MessageBox.Action.YES,
				onClose: function (sAction) {
					if (sAction === "YES") {
						var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
						var bpData = oStorage.get("BusinessPartner.LocalData");
						that.getView().getModel("appModel").setProperty("/AppData", {
							"actionCreateALike": true,
							"actionCreate": true
						});
						that.getOwnerComponent().getRouter().navTo("object", {
							objectId: "-1"
						}, true);
					} else {
						jQuery.sap.storage(jQuery.sap.storage.Type.local).remove("BusinessPartner.LocalData");
						bpData = {};
						bpData.generalData = {};
						bpData.address = {};
						bpData.company = [];
						that.getView().getModel("appModel").setProperty("/AppData", {
							"actionCreateALike": false,
							"actionCreate": true
						});
					}
					that.getView().setModel(new JSONModel(bpData), "pModel");
					that.getOwnerComponent().getRouter().navTo("object", {
						objectId: "-1"
					}, true);
				}
			});
		},
		onPressClear: function (oEvent) {
			this.getView().getModel("filterModel").setProperty("/filterData", {});
		},
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.SearchBP
		 */
		//	onBeforeRendering: function() {
		//
		//	},
		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.SearchBP
		 */
		// onAfterRendering: function () {
		// },
		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.SearchBP
		 */
		onExit: function () {},
		/**
		 *@memberOf spar.bp.create.starter.BPCreate_Starter.controller.SearchBP
		 */
		action: function (oEvent) {
			var that = this;
			var actionParameters = JSON.parse(oEvent.getSource().data("wiring").replace(/'/g, "\""));
			var eventType = oEvent.getId();
			var aTargets = actionParameters[eventType].targets || [];
			aTargets.forEach(function (oTarget) {
				var oControl = that.byId(oTarget.id);
				if (oControl) {
					var oParams = {};
					for (var prop in oTarget.parameters) {
						oParams[prop] = oEvent.getParameter(oTarget.parameters[prop]);
					}
					oControl[oTarget.action](oParams);
				}
			});
			var oNavigation = actionParameters[eventType].navigation;
			if (oNavigation) {
				var oParams = {};
				(oNavigation.keys || []).forEach(function (prop) {
					oParams[prop.name] = encodeURIComponent(JSON.stringify({
						value: oEvent.getSource().getBindingContext(oNavigation.model).getProperty(prop.name),
						type: prop.type
					}));
				});
				if (Object.getOwnPropertyNames(oParams).length !== 0) {
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName, oParams);
				} else {
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName);
				}
			}
		}

	});
});