sap.ui.define([], function () {
	"use strict";
	return {
		removetime: function (value) {
			if (value) {
				var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
					pattern: "MM-dd-yyyy"
				});
				return oDateFormat.format(new Date(value));
			} else {
				return value;
			}
		}
	};
});